package sweng.shared;


/* Classe get/set che gestisce tramite un token[username] la sessione dell'utente */
public class Session {
	
	protected String session_username;
	protected String session_name;
	
	public Session(){
		session_username = null;
		session_name = null;
	}
	
	// Imposta il nome utente della sessione
	public void setSession_username(String username){
		session_username = username;
	}
	
	// Prende il nome utente della sessione
	public String getSession_username(){
		return session_username;
	}
	
	// Prende il nome dell'utente della sessione
	public void setSession_name(String name){	
		session_name = name;
	}
	
	public String getSession_name(){
		return session_name;
	}
	
	// Controlla se la sessione � valida, verificando che l'username sia stato impostato
	public boolean isValid(){
		if(session_username == null)
			return false;
		else
			return true;
	}
	
	public void reset(){
		session_username = null;
		session_name = null;
	}
}
