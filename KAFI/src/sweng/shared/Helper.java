package sweng.shared;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

/* Classe che implementa funzioni utili per mantenere il codice pulito e leggibile */
public class Helper {
	
	// Ritorna un ID casuale *unico* di 7 caratteri
	public static String generate_random_string(){
		ResultSet stringQuery_result = null;
		
		Random ran = new Random();
		int string_lenght = 6;
		char data = ' ';
		
		String dat = ""; // Stringa che conterra' l'identificatore finale

		
		// Ciclo che genera un ID unico.
		// Controlla che l'ID generato sia unico tramite una SELECT sulla tabella Eventi
		// altrimenti ripete il ciclo generando un nuovo ID
		do{
			for (int i=0; i <= string_lenght; i++) {
			  data = (char)(ran.nextInt(25)+97);
			  dat = data + dat;
			}
			
			ArrayList<String> List_db_query = new ArrayList<String>();
			List_db_query.clear();
			List_db_query.add(dat);
			
			// QUERY 
			// [Tabella: Eventi]
			// [Operazione: SELECT]
			// ritorna un ResultSet o null se ha trovato/non trovato il record			
			stringQuery_result = Database.select_query(
						"SELECT * FROM Eventi WHERE id = ?",
						1,
						List_db_query);
			
		// Ripete il ciclo finch� l'ID generato non viene trovato nella tabella Eventi 
		}while(stringQuery_result != null);
		
		return dat;
	}
	
	
	// Controlla se data una lista [List], esiste l'elemento passato nella lista[element]
	// Ritorna TRUE se element � presente nella lista, FALSE altrimenti
	public static Boolean check_if_element_exist_in_list(String element, LinkedList<String> List){
		
		// Creo l'iteratore della lista
		Iterator<String> iter = List.listIterator();
		
		String list_element;
		
		do{
			list_element = iter.next();
			
			if( element == list_element){
				return true;
			}
			
		}while( iter.hasNext() );
		
		return false;
		
	}

}
