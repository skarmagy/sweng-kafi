package sweng.shared;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

/* Classe che gestisce il database PostgreSQL */
public class Database {
	
	// Data una query, il numero di campi e una lista con i record, per ogni campo effettua la INSERT
	// Ritorna "ok" se � andato tutto bene, "errore" altrimenti
	public static String insert_query(String query, int records_number, ArrayList<String> list){
		
		// Creo un iteratore alla lista che servir� per scorrere quest'ultima
		Iterator<String> iter = list.listIterator();
				
		// URL del database
		String url = "jdbc:postgresql://127.0.0.1:5432/postgres";
		
		try {
			Class.forName("org.postgresql.Driver");//Caricamento dei driver postgreSQL
		} catch (ClassNotFoundException e1) {
			// Errore #Database 1 [Impossibile caricare i driver JDBC]
			return "Fatal error #Database 1";
		}	
		
		try {
			// Si connette al database
			Connection conn = DriverManager.getConnection(url,"postgres","postgres");
			
			// Si prepara ad effettuare la query
			PreparedStatement pst = conn.prepareStatement(query);
			
			// Contatore che servir� come indice della lista
			int cont = 1;
			
			do{
				// Sostituisco i placeholder (?) della query con i dati inseriti nella lista
				pst.setString(cont, iter.next());
				cont++;
			}while(cont <= records_number);
			
			// Si esegue la query
			pst.executeUpdate();
			
			// Chiusura della connessione con il database
			conn.close();
			
		} catch (SQLException e) {
			// Errore #Database 2 [Impossibile eseguire la INSERT]
			return "Fatal error #Database 2";
		}
		
		return "ok";
	}


	// Data una query, il numero di campi e una lista con i record, effettua la SELECT
	// Ritorna null se non � stato trovato nulla, una ResultSet altrimenti
	public static ResultSet select_query(String query, int records_number, ArrayList<String> list){
		
		// Conterr� il risultato della query
		ResultSet resultSet = null;
		
		// Creo un iteratore alla lista che servir� per scorrere quest'ultima
		Iterator<String> iter = list.listIterator();
				
		// URL del database
		String url = "jdbc:postgresql://127.0.0.1:5432/postgres";
		
		try {
			Class.forName("org.postgresql.Driver");//Caricamento dei driver postgreSQL
		} catch (ClassNotFoundException e1) {
			// Errore #Database 1 [Impossibile caricare i driver JDBC]
			return resultSet;
		}	
		
		try {
			// Si connette al database
			Connection conn = DriverManager.getConnection(url,"postgres","postgres");

			// Si prepara ad effettuare la query
			PreparedStatement pst = conn.prepareStatement(query);
			
			// Contatore che servir� come indice della lista
			int cont = 1;
			
			do{
				// Sostituisco i placeholder (?) della query con i dati inseriti nella lista
				pst.setString(cont, iter.next());
				cont++;
			}while(cont <= records_number);
			
			// Si esegue la query
			resultSet = pst.executeQuery();
			
			// Se il risultato della query � andata a buon fine ritorna il risultato della query
			if ( resultSet.next() ){
				conn.close();
				return resultSet;
			}
			
			// Chiusura della connessione con il database
			conn.close();
			
		} catch (SQLException e) {
			return resultSet;
		}
		
		resultSet = null;
		return resultSet;
		
	}

	
	// Data una query, il numero di campi e una lista con i record, per ogni campo effettua la UPDATE
	// Ritorna "ok" se � andato tutto a buon fine, "errore" altrimenti
	public static String update_query(String query, int records_number, ArrayList<String> list){
		
		// Creo un iteratore alla lista che servir� per scorrere quest'ultima
		Iterator<String> iter = list.listIterator();
				
		// URL del database
		String url = "jdbc:postgresql://127.0.0.1:5432/postgres";
		
		try {
			Class.forName("org.postgresql.Driver");//Caricamento dei driver postgreSQL
		} catch (ClassNotFoundException e1) {
			// Errore #Database 1 [Impossibile caricare i driver JDBC]
			return "Fatal error #Database 1";
		}	
		
		try {
			// Si connette al database
			Connection conn = DriverManager.getConnection(url,"postgres","postgres");

			// Si prepara ad effettuare la query
			PreparedStatement pst = conn.prepareStatement(query);
			
			// Contatore che servir� come indice della lista
			int cont = 1;
			
			do{
				// Sostituisco i placeholder (?) della query con i dati inseriti nella lista
				pst.setString(cont, iter.next());
				cont++;
			}while(cont <= records_number);
			
			// Si esegue la query
			pst.executeUpdate();
			
			// Chiusura della connessione con il database
			conn.close();
			
		} catch (SQLException e) {
			// Errore #Database 3 [Impossibile eseguire la UPDATE]
			return "Fatal error #Database 3";
		}
		
		return "ok";
	}

	// Data una query, il numero di campi e una lista con i record, per ogni campo effettua la DELETE
	// Ritorna "ok" se � andato tutto a buon fine, "errore" altrimenti
	public static String delete_query(String query, int records_number, ArrayList<String> list){
			
			// Creo un iteratore alla lista che servir� per scorrere quest'ultima
			Iterator<String> iter = list.listIterator();
					
			// URL del database
			String url = "jdbc:postgresql://127.0.0.1:5432/postgres";
			
			try {
				Class.forName("org.postgresql.Driver");//Caricamento dei driver postgreSQL
			} catch (ClassNotFoundException e1) {
				// Errore #Database 1 [Impossibile caricare i driver JDBC]
				return "Fatal error #Database 1";
			}	
			
			try {
				// Si connette al database
				Connection conn = DriverManager.getConnection(url,"postgres","postgres");

				// Si prepara ad effettuare la query
				PreparedStatement pst = conn.prepareStatement(query);
				
				// Contatore che servir� come indice della lista
				int cont = 1;
				
				do{
					// Sostituisco i placeholder (?) della query con i dati inseriti nella lista
					pst.setString(cont, iter.next());
					cont++;
				}while(cont <= records_number);
				
				// Si esegue la query
				pst.executeUpdate();
				
				// Chiusura della connessione con il database
				conn.close();
				
			} catch (SQLException e) {
				// Errore #Database 4 [Impossibile eseguire la UPDATE]
				return "Fatal error #Database 4";
			}
			
			return "ok";
		}
}
