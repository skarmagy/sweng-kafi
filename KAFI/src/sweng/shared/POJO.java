package sweng.shared;

import java.io.Serializable;
import java.util.LinkedList;

/* Classe POJO istanziata dal server e ritornata al client durante la comunicazione RPC */
public class POJO implements Serializable {
	
    private static final long serialVersionUID = 1L;
	
    // 0 -> Operazione pianificata non riuscita 
    // 1 -> Operazione pianificata riuscita
	protected int response;

	// Messaggio del server al client
	protected String content;
	
	// Lista di liste contenente dei dati elaborati dal server
	protected LinkedList<LinkedList<String>> responseList;
	
	public POJO(){}
	
	
	// Ritorna 1 se il server risponde con successo all'operazione previsa
	// 0 altrimenti
	public int getServerResponse(){
		return response;
	}
	
	// Ritorna il messaggio del server al client
	public String getServerResponseMessage(){
		return content;
	}
	
	// Ritorna dei dati elaborati dal server tramite una lista di liste
	public LinkedList<LinkedList<String>> getServerResponseList(){
		return responseList;
	}
	
	// Ritorna una stringa precedentemente impostata
	public String getStringData(){
		return content;
	}
	
	// Imposta la risposta del server
	public void setServerResponse(int value){
		response = value;
	}
	
	// Imposta il messaggio di risposta del server
	public void setServerResponseMessage(String message){
		content = message;
	}
	
	// Imposta la lista di liste contenente i dati elaborati dal server
	public void setServerResponseList(LinkedList<LinkedList<String>> list){
		responseList = list;
	}
	
	// Imposta una stringa qualsiasi
	public void setStringData(String data){
		content = data;
	}

}
