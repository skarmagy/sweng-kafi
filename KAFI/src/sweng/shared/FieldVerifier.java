package sweng.shared;

import java.util.Iterator;
import java.util.LinkedList;

import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;


/* Classe che si occupa di validare i dati delle form */
public class FieldVerifier {
		
	// Data una stringa, una lunghezza minima e una massima
	// Valuta se la lunghezza della stringa � compresa fra quelle passate
	public static String validateStringFormat(String text, int min_lenght, int max_lenght){
		
		if( text.length() < min_lenght)
			return "is too short (min " + min_lenght + ")";
		
		if(text.length() > max_lenght)
			return "is too long (min " + max_lenght + ")";
		
		return "ok";
	}
	
	// Ritorna una [lista di liste] con tutti gli elementi della form CreateEvent
	// Vengono fatti dei primi controlli sull'integrit� dei dati
	public static LinkedList<LinkedList<String>> CreateEvent_validate_client_side(
			VerticalPanel TopPanel, VerticalPanel MiddlePanel, Label labelError, String username){
		
		// Lista di liste
		// L'unico modo per gestire dinamicamente tante date a cui sono associati
		// tanti orari � quello di usare liste di liste
		LinkedList<LinkedList<String>> list = new LinkedList<LinkedList<String>>();
		
		// Sar� ritornata una lista vuota se avviene un errore
		LinkedList<LinkedList<String>> empty_list = new LinkedList<LinkedList<String>>();
		
		// Prima lista che conterr� il nome,il luogo e la descrizione dell'evento		
		list.add(new LinkedList<String>());
		
		TextBox textboxEventName = (TextBox) TopPanel.getWidget(1);
		TextBox textboxEventPlace = (TextBox) TopPanel.getWidget(3);
		TextArea textareaEventDescription = (TextArea) TopPanel.getWidget(5);
		
		list.get(0).add(username);
		list.get(0).add(textboxEventName.getText());
		list.get(0).add(textboxEventPlace.getText());
		list.get(0).add(textareaEventDescription.getText());
		
		
		// CONTROLLO: Errore se non viene impostata nemmeno una data per l'evento
		if( MiddlePanel.getWidgetCount() == 1){
			labelError.setText("You must insert at least one date!");
			return empty_list;
		}
		
		
		HorizontalPanel hpanelDateBox_DateBox;
		Widget dateBox_widget;
		TextBox dateBox_textbox;
		HorizontalPanel hpanelDateBox_Hours;
		Widget hourBox_widget;
		String hourBox_textbox;
		
		// Contatore alla posizione del Widget che preleveremo dalla MiddlePanel
		int widget_cont = 2;
		
		// Contatore al numero della TextBox dell'orario
		int hours_cont = 1;
		
		// Contatore al numero della lista
		int list_cont = 0;
		
		// Inizio gestione dinamica delle date e degli orari
		
		do{
		
			list_cont++;
			
			// Creazione della lista
			list.add(new LinkedList<String>());	
			
			// Estrapolo il pannello in cui c'e la datebox
			hpanelDateBox_DateBox = (HorizontalPanel) MiddlePanel.getWidget( widget_cont );
			// Estrapolo la DateBox
			dateBox_widget = hpanelDateBox_DateBox.getWidget(1);
			// Prendo la TextBox della DateBox
			dateBox_textbox = ((DateBox) dateBox_widget).getTextBox();
			
			// CONTROLLO: Errore se non inserisco una data corretta
			if( dateBox_textbox.getValue() == "" || dateBox_textbox.getValue() == " "){
				labelError.setText("Insert valid date for this event!");
				return empty_list;
			}
			
			// Inserisco la data dell'evento nella lista
			list.get(list_cont).add(dateBox_textbox.getText());
			
			
			// Incremento il contatore che mi servir� per posizionarmi sul widget degli orari
			widget_cont = widget_cont + 2;
			
			// Estrapolo le textbox dei vari orari
			hpanelDateBox_Hours = (HorizontalPanel) MiddlePanel.getWidget( widget_cont );
			
			do{
				// Estrapolo la TextBox
				hourBox_widget = hpanelDateBox_Hours.getWidget( hours_cont );
				// Estrapolo l'ora dalla TextBox
				hourBox_textbox = ((TextBox) hourBox_widget).getValue();
		
				
				// CONTROLLO: Errore se non inserisco un orario
				if( hourBox_textbox == "" || hourBox_textbox == " "){
					labelError.setText("Insert valid hours for this event!");
					return empty_list;
				}
				
				// CONTROLLO: Errore se l'orario inserito � troppo lungo
				if( hourBox_textbox.length() > 7){
					labelError.setText("The text for the hour is too long!");
					return empty_list;
				}
				
				// Inserisco l'ora dell'evento nella lista
				list.get(list_cont).add(hourBox_textbox);
				
				hours_cont++;
			
			}while(hpanelDateBox_Hours.getWidgetCount() > hours_cont);
			 
			// Incremento il contatore che mi servir� per posizionarmi sull'eventuale widget delle date
			widget_cont = widget_cont + 2;
			
			hours_cont = 1;
		
		}while((MiddlePanel.getWidgetCount()) > widget_cont); 
		// WHILE: Se il mio indice "widget_cont" supera il numero di widget allora termino il ciclo
		
		return list;
	}

	
	// Ritorna una [lista di liste] con tutti gli elementi della form CreateEvent
	// Vengono fatti tutti i restanti controlli sull'integrit� dei dati
	public static String CreateEvent_validate_server_side(
			LinkedList<LinkedList<String>> list){
		
		if(list == null){
			return "FATAL ERROR: LIST IS EMPTY";
		}
		
		// Creo un iteratore alla lista principale che servir� per scorrere gli elementi della lista
		Iterator<LinkedList<String>> MainList_iter = list.listIterator();
		
		// Punto l'iteratore della lista principale alla prima lista
		LinkedList<String> DinamicList = MainList_iter.next();
		
		// Creo un iteratore alla lista figlia puntata dall'iteratore principale
		Iterator<String> SubList_iter = DinamicList.listIterator();
		
		// Salto l'username
		SubList_iter.next();
		
		String EventName = SubList_iter.next();
		String EventPlace = SubList_iter.next();
		String EventDescription = SubList_iter.next();
		
		
		// Controllo che il nome dell'evento sia lungo almeno 3 caratteri e non pi� lungo di 20
		if( EventName.length() < 3 ){
			return "Event name too short (min 3)";
		}
		
		if( EventName.length() > 20 ){
			return "Event name too long (max 20)";
		}
		
		// Controllo che il luogo dell'evento sia lungo almeno 3 caratteri e non pi� lungo di 20
		if( EventPlace.length() < 5 ){
			return "Event place too short (min 5)";
		}
		if( EventPlace.length() > 20 ){
			return "Event place too long (max 20)";
		}
		
		// Controllo se � stata inserita una descrizione all'evento
		if( EventDescription.length() != 0){
			// Controllo che la descrizione sia lunga almeno 3 caratteri e non pi� di 50
			if( EventDescription.length() < 5){
				return "Event description too short (min 5)";
			}
			if( EventDescription.length() > 50){
				return "Event description too long (max 50)";
			}
		}
		
		
		String EventDate;
		String EventHours;
		
		// Lista d'appoggio per controllare se l'utente ha inserito due date per l'evento uguali
		LinkedList<String> ListOfDates = new LinkedList<String>();
		
		// Lista d'appoggio per controllare se l'utente ha inserito due orari uguali per la stessa data
		LinkedList<String> ListOfHours = new LinkedList<String>();
		
		Boolean IgnoredFirstIteraction_Date = false;
		Boolean IgnoredFirstIteraction_Hours = false;
		
		
		// Ciclo do-while che controlla la correttezza delle date e degli orari
		do{
			// Posizioniamo l'iteratore alla lista successiva, quella che contiene la
			// la data dell'evento e i suoi orari
			DinamicList = MainList_iter.next();
			SubList_iter = DinamicList.listIterator();
			
			// Stringa che contiene la data dell'evento
			EventDate = SubList_iter.next();
			
			// Controllo che la data inserita non sia gi� stata inserita per questo evento
			if( IgnoredFirstIteraction_Date && Helper.check_if_element_exist_in_list(EventDate, ListOfDates)){
				return "You inserted two or more same dates for the event";
			}
			ListOfDates.add(EventDate);

			
			do{
				EventHours = SubList_iter.next();
				
				// Controllo che la data inserita non sia gi� stata inserita per questo evento
				if( IgnoredFirstIteraction_Hours && Helper.check_if_element_exist_in_list(EventHours, ListOfHours)){
					return "You inserted two or more same hours for a Date";
				}
				ListOfHours.add(EventHours);
				
				IgnoredFirstIteraction_Hours = true;
				
			}while(SubList_iter.hasNext());	
			
			ListOfHours.clear();
			IgnoredFirstIteraction_Hours = false;
			IgnoredFirstIteraction_Date = true;
			
		}while(MainList_iter.hasNext());
		
		
		return "ok";
	}
	
	// Ritorna un "ok" se la validazione della form di Login ha avuto successo
	// La stringa di errore altrimenti
	public static String Login_validate_server_side(String[] input){
		
		String username = input[0];
		String password = input[1];
		
		// Controllo che l'Username sia lungo almeno 7 caratteri e non pi� lungo di 15
		if( username.length() < 7 ){
			return "Username too short (min 7)";
		}
		if( username.length() > 15){
			return "Username too long (max 15)";
		}
		
		// Controllo che la Password sia lunga almeno 7 caratteri e non pi� lunga di 15
		if( password.length() < 7){
			return "Password too short (min 7)";
		}
		if( password.length() > 15){
			return "Password too long (max 15)";
		}
		
		return "ok";
	}

	// Ritorna un "ok" se la validazione della form di Registrazione ha avuto successo
	// La stringa di errore altrimenti
	public static String Register_validate_server_side(String[] input){
		
		String username = input[0];
		String password = input[1];
		String confirm_password = input[2];
		String name = input[3];
		String email = input[4];
		

		// Controllo che l'Username sia lungo almeno 7 caratteri e non pi� lungo di 15
		if( username.length() < 7 ){
			return "Username too short (min 7)";
		}
		if( username.length() > 15){
			return "Username too long (max 15)";
		}
		
		// Controllo che la Password e la ConfirmPassword siano uguali
		if( password != confirm_password){
			return "Passwords do not match";
		}
		// Controllo che la Password sia lunga almeno 7 caratteri e non piu lunga di 15
		if( password.length() < 7){
			return "Password too short (min 7)";
		}
		if( password.length() > 15){
			return "Password too long (max 15)";
		}
		
		
		// Controllo che il nome sia lungo almeno 3 caratteri e non pi� di 15
		if( name.length() < 3){
			return "Name too short (min 3)";
		}
		if( name.length() > 15){
			return "Name too long (max 15)";
		}
		
		
		// Controllo se � stata inserita una Email
		if( email.length() != 0){
			
			// Controllo che non sia troppo lunga
			if( email.length() > 40){
				return "Email too long (max 40)";
			}
			
			// Controllo che l'email sia nel formato corretto (es. x@x.it e non @x)
			Boolean EmailFormat_isCorrect_bool = email.matches("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
			
			if( EmailFormat_isCorrect_bool == false){
				return "Wrong email format";
			}
		}
		
		return "ok";
	}
}
