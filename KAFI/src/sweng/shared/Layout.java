package sweng.shared;

import sweng.client.Main;
import sweng.client.event.create.CreateEvent;
import sweng.client.event.myevents.MyEvents;
import sweng.client.popup.PopupMain;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.RootPanel;

/* Classe che gestisce funzioni per la grafica dell'applicazione JME */
public class Layout {
	
	// Pulisce la pagina da tutti gli elementi aggiunti da GWT
	public static void clean_everything(){
		clean_everything_in_html_menu_bar();
		clean_everything_in_html_main_content();
		RootPanel.get("html_labelUsername").clear();
		RootPanel.get("html_pushbuttonLogout").clear();
	}
	
	// Pulisce la HTML_MENU_BAR da tutti gli elementi aggiunti da GWT
	public static void clean_everything_in_html_menu_bar(){
		RootPanel.get("html_pushbuttonRegister").clear();
		RootPanel.get("html_pushbuttonLogin").clear();
		RootPanel.get("html_pushbuttonCreateEvent").clear();
		RootPanel.get("html_pushbuttonMyEvents").clear();
		RootPanel.get("html_pushbuttonSearchEvent").clear();
	}

	// Pulisce la HTML_MAIN_CONTENT da tutti gli elementi aggiunti da GWT
	public static void clean_everything_in_html_main_content(){
		RootPanel.get("html_main_content_top").clear();
		RootPanel.get("html_main_content_middle").clear();
		RootPanel.get("html_main_content_bottom").clear();
	}
	
	// Pulisce la HTML_MAIN_CONTENT_TOP, che contiene il titolo del contenuto della pagina
	public static void clean_html_main_content_top(){
		RootPanel.get("html_main_content_top").clear();
	}
	
	// Pulisce la HTML_MAIN_CONTENT_MIDDLE, che contiene il contenuto della pagina
	public static void clean_html_main_content_middle(){
		RootPanel.get("html_main_content_middle").clear();
	}
	
	// Pulisce la HTML_MAIN_CONTENT_BOTTOM, che contiene la labelError della pagina
	public static void clean_html_main_content_bottom(){
		RootPanel.get("html_main_content_bottom").clear();
	}
	
	// Aggiunge la label che conterr� il nome dell'utente nella USER_BAR
	public static Label add_html_labelUsername(){
		final Label labelUsername = new Label();
		labelUsername.addStyleName("css-user_bar_labelUsername");
		
		RootPanel.get("html_labelUsername").clear();
		RootPanel.get("html_labelUsername").add(labelUsername);
		
		return labelUsername;
	}
	
	// Aggiunge la label che conterr� il titolo del contenuto della pagina
	public static void add_title(String Title){
		
		Label labelTitle = new Label(Title);
		RootPanel.get("html_main_content_top").add(labelTitle);
		labelTitle.setStyleName("css-generics_toplabel");
	}
	
	// Aggiunge la label che conterr� l'errore del contenuto della pagina
	public static Label add_labelError(){
		
		Label labelError = new Label(" ");
		RootPanel.get("html_main_content_bottom").add(labelError);
		labelError.setStyleName("css-generics_labelerror");
		
		return labelError;
	}	
	
	// Aggiunge il bottone "SIGN UP" nella MENU_BAR
	public static void add_html_pushbuttonRegister(final Session activeSession){
		final PushButton pushbuttonRegister = new PushButton();				
		pushbuttonRegister.setText(" SIGN UP ");
		pushbuttonRegister.setStyleName("css-user_bar_button");
		RootPanel.get("html_pushbuttonRegister").add(pushbuttonRegister);
		
		/* Instanzio il popup della registrazione quando clicco sul bottone Sign up */
		pushbuttonRegister.addClickHandler(new ClickHandler(){
			public void onClick(ClickEvent event) {
				
				// Visualizza il popup di registrazione al centro della pagina
				PopupMain.createPopupRegistration(activeSession);
			}
		});
	}
	
	// Aggiunge il bottone "LOG IN" nella MENU_BAR
	public static void add_html_pushbuttonLogin(final Session activeSession){
		final PushButton pushbuttonLogin = new PushButton();			
		pushbuttonLogin.setText("LOG IN");
		pushbuttonLogin.setStyleName("css-user_bar_button");
		RootPanel.get("html_pushbuttonLogin").add(pushbuttonLogin);
		
		/* Instanzio il popup della login quando clicco sul bottone Log in */
		pushbuttonLogin.addClickHandler(new ClickHandler(){
			public void onClick(ClickEvent event) {
				
				// Visualizza il popup di login al centro della pagina
				PopupMain.createPopupLogin(activeSession);
			}
		});
	}
	
	// Aggiunge il bottone "SEARCH EVENT" nella MENU_BAR
	public static void add_html_pushbuttonSearchEvent(final Session activeSession){
		// Aggiungo il bottone "SEARCH EVENT"
		final PushButton pushbuttonSearchEvent = new PushButton();				
		pushbuttonSearchEvent.setText("SEARCH EVENT");
		pushbuttonSearchEvent.setStyleName("css-user_bar_button");
		RootPanel.get("html_pushbuttonSearchEvent").add(pushbuttonSearchEvent);
		
		/* Aggiungo un clickHandler all'hyperlink "Create Event" */
		pushbuttonSearchEvent.addClickHandler(new ClickHandler(){
			public void onClick(ClickEvent event) {
				
				// Visualizza il popup SearchEvent al centro della pagina
				PopupMain.createPopupSearchEvent(activeSession);
			}
		});
	}
	
	// Aggiunge il bottone "CREATE EVENT" nella MENU_BAR
	public static void add_html_pushbuttonCreateEvent(final Session activeSession){
		// Aggiungo il bottone "Create Event"
		final PushButton pushbuttonCreateEvent = new PushButton();				
		pushbuttonCreateEvent.setText("CREATE EVENT");
		pushbuttonCreateEvent.setStyleName("css-user_bar_button");
		RootPanel.get("html_pushbuttonCreateEvent").add(pushbuttonCreateEvent);
		
		/* Aggiungo un clickHandler all'hyperlink "Create Event" */
		pushbuttonCreateEvent.addClickHandler(new ClickHandler(){
			public void onClick(ClickEvent event) {
				
				// Istanzio l'oggetto CreateEvent
				CreateEvent CreateEventIstance = new CreateEvent(activeSession);
				
				// Visualizzo la form di creazione dell'evento
				CreateEventIstance.form();
			}
		});
	}
	
	// Aggiunge il bottone "MY EVENTS" nella MENU_BAR
	public static void add_html_pushbuttonMyEvents(final Session activeSession){
		// Aggiungo il bottone "MyEvents"
		final PushButton pushbuttonMyEvents = new PushButton();				
		pushbuttonMyEvents.setText(" MY EVENTS ");
		pushbuttonMyEvents.setStyleName("css-user_bar_button");
		RootPanel.get("html_pushbuttonMyEvents").add(pushbuttonMyEvents);
		
		/* Aggiungo un clickHandler all'hyperlink "Create Event" */
		pushbuttonMyEvents.addClickHandler(new ClickHandler(){
			public void onClick(ClickEvent event) {
				
				// Istanzio l'oggetto MyEvents
				MyEvents MyEventsIstance = new MyEvents(activeSession);
				MyEventsIstance.show_my_events_list();
			}
		});
	}
	
	// Aggiunge il bottone "LOGOUT" nella LOGOUT_BAR
	public static void add_html_pushbuttonLogout(final Session activeSession){
		
		// Aggiungo il bottone "Logout"
		final PushButton pushbuttonLogout = new PushButton();				
		pushbuttonLogout.setText(" LOGOUT ");
		pushbuttonLogout.setStyleName("css-html_pushbuttonLogout");
		RootPanel.get("html_pushbuttonLogout").add(pushbuttonLogout);
		
		/* Aggiungo un clickHandler all'hyperlink "Create Event" */
		pushbuttonLogout.addClickHandler(new ClickHandler(){
			public void onClick(ClickEvent event) {
				
				activeSession.reset();
				
				// Elimino dalla RootPanel il bottone "Logout"
				RootPanel.get("html_pushbuttonLogout").clear();
				
				// Istanzio un nuovo oggetto Main
				Main NewMainIstance = new Main();
				NewMainIstance.onModuleLoad();
			}
		});
	}
}
