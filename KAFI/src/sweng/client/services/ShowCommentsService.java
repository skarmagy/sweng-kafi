package sweng.client.services;

import sweng.shared.POJO;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;


@RemoteServiceRelativePath("show_comments")
public interface ShowCommentsService extends RemoteService {
	POJO show_commentsServer(String EventID) throws IllegalArgumentException;
}
