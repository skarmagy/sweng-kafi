package sweng.client.services;

import java.util.LinkedList;

import sweng.shared.POJO;

import com.google.gwt.user.client.rpc.AsyncCallback;


public interface CreateEventServiceAsync {
	void createeventServer(LinkedList<LinkedList<String>> input, AsyncCallback<POJO> callback)
			throws IllegalArgumentException;
}
