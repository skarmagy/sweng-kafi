package sweng.client.services;

import sweng.shared.POJO;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;


@RemoteServiceRelativePath("delete_event")
public interface DeleteEventService extends RemoteService {
	POJO deleteeventServer(String EventID) throws IllegalArgumentException;
}
