package sweng.client.services;

import java.util.LinkedList;

import sweng.shared.POJO;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;



@RemoteServiceRelativePath("create_event")
public interface CreateEventService extends RemoteService {
	POJO createeventServer(LinkedList<LinkedList<String>> input) throws IllegalArgumentException;
}
