package sweng.client.services;

import sweng.shared.POJO;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;


@RemoteServiceRelativePath("my_events")
public interface MyEventsService extends RemoteService {
	POJO myeventsServer(String username) throws IllegalArgumentException;
}
