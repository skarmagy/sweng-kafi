package sweng.client.services;

import sweng.shared.POJO;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;


@RemoteServiceRelativePath("close_event")
public interface CloseEventService extends RemoteService {
	POJO closeeventServer(String[] data) throws IllegalArgumentException;
}
