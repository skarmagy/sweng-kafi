package sweng.client.services;

import sweng.shared.POJO;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;


@RemoteServiceRelativePath("create_comment")
public interface CreateCommentService extends RemoteService {
	POJO create_commentServer(String[] data) throws IllegalArgumentException;
}
