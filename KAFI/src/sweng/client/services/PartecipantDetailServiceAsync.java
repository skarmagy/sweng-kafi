package sweng.client.services;

import sweng.shared.POJO;

import com.google.gwt.user.client.rpc.AsyncCallback;


public interface PartecipantDetailServiceAsync {
	void partecipant_detailServer(String[] input, AsyncCallback<POJO> callback)
			throws IllegalArgumentException;
}
