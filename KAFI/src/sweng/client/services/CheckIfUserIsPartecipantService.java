package sweng.client.services;

import sweng.shared.POJO;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;


@RemoteServiceRelativePath("check_if_user_is_partecipant")
public interface CheckIfUserIsPartecipantService extends RemoteService {
	POJO check_if_user_is_partecipantServer(String[] data) throws IllegalArgumentException;
}
