package sweng.client.services;

import sweng.shared.POJO;

import com.google.gwt.user.client.rpc.AsyncCallback;


public interface PartecipantServiceAsync {
	void partecipantServer(String[] input, AsyncCallback<POJO> callback)
			throws IllegalArgumentException;
}
