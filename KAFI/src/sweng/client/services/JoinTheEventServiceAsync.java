package sweng.client.services;

import java.util.LinkedList;

import sweng.shared.POJO;

import com.google.gwt.user.client.rpc.AsyncCallback;


public interface JoinTheEventServiceAsync {
	void join_the_eventServer(LinkedList<LinkedList<String>> List, AsyncCallback<POJO> callback)
			throws IllegalArgumentException;
}
