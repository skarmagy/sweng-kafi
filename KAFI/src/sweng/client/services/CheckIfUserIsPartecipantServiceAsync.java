package sweng.client.services;

import sweng.shared.POJO;

import com.google.gwt.user.client.rpc.AsyncCallback;


public interface CheckIfUserIsPartecipantServiceAsync {
	void check_if_user_is_partecipantServer(String[] data, AsyncCallback<POJO> callback)
			throws IllegalArgumentException;
}
