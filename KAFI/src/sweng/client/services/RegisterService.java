package sweng.client.services;

import sweng.shared.POJO;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;


@RemoteServiceRelativePath("register")
public interface RegisterService extends RemoteService {
	POJO registerServer(String[] input) throws IllegalArgumentException;
}
