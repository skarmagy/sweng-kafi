package sweng.client.services;

import sweng.shared.POJO;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("search_event")
public interface SearchEventService extends RemoteService{
	POJO searcheventServer(String EventID) throws IllegalArgumentException;
}
