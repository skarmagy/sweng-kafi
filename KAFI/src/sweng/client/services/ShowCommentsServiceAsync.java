package sweng.client.services;

import sweng.shared.POJO;

import com.google.gwt.user.client.rpc.AsyncCallback;


public interface ShowCommentsServiceAsync {
	void show_commentsServer(String EventID, AsyncCallback<POJO> callback)
			throws IllegalArgumentException;
}
