package sweng.client.services;

import sweng.shared.POJO;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface SearchEventServiceAsync {
	void searcheventServer(String EventID, AsyncCallback<POJO> callback) 
			throws IllegalArgumentException;
}