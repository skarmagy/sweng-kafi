package sweng.client.services;

import sweng.shared.POJO;

import com.google.gwt.user.client.rpc.AsyncCallback;


public interface CloseEventServiceAsync {
	void closeeventServer(String[] data, AsyncCallback<POJO> callback)
			throws IllegalArgumentException;
}
