package sweng.client.services;

import sweng.shared.POJO;

import com.google.gwt.user.client.rpc.AsyncCallback;


public interface RegisterServiceAsync {
	void registerServer(String[] input, AsyncCallback<POJO> callback)
			throws IllegalArgumentException;
}
