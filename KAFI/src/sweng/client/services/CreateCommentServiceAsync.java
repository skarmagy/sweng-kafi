package sweng.client.services;

import sweng.shared.POJO;

import com.google.gwt.user.client.rpc.AsyncCallback;


public interface CreateCommentServiceAsync {
	void create_commentServer(String[] data, AsyncCallback<POJO> callback)
			throws IllegalArgumentException;
}
