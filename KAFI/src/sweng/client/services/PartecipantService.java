package sweng.client.services;

import sweng.shared.POJO;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;


@RemoteServiceRelativePath("partecipant")
public interface PartecipantService extends RemoteService {
	POJO partecipantServer(String[] input) throws IllegalArgumentException;
}