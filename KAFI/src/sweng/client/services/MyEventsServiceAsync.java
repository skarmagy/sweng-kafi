package sweng.client.services;

import sweng.shared.POJO;
import com.google.gwt.user.client.rpc.AsyncCallback;


public interface MyEventsServiceAsync {
	void myeventsServer(String username, AsyncCallback<POJO> callback)
			throws IllegalArgumentException;
}