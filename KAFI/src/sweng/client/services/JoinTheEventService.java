package sweng.client.services;

import java.util.LinkedList;

import sweng.shared.POJO;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;


@RemoteServiceRelativePath("join_the_event")
public interface JoinTheEventService extends RemoteService {
	POJO join_the_eventServer(LinkedList<LinkedList<String>> List) throws IllegalArgumentException;
}
