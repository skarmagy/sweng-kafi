package sweng.client.services;

import sweng.shared.POJO;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;


@RemoteServiceRelativePath("partecipant_detail")
public interface PartecipantDetailService extends RemoteService {
	POJO partecipant_detailServer(String[] input) throws IllegalArgumentException;
}