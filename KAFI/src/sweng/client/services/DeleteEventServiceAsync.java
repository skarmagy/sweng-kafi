package sweng.client.services;

import sweng.shared.POJO;

import com.google.gwt.user.client.rpc.AsyncCallback;


public interface DeleteEventServiceAsync {
	void deleteeventServer(String EventID, AsyncCallback<POJO> callback)
			throws IllegalArgumentException;
}
