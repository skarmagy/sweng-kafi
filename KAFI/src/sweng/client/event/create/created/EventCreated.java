package sweng.client.event.create.created;

import sweng.client.Main;
import sweng.shared.Layout;
import sweng.shared.Session;

import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;


/* Classe che viene chiamata dalla classe CreateEvent dopo aver con successo creato un evento */
public class EventCreated {

	// Dati della sessione dell'utente
	protected Session activeSession;
	
	
	// Vengono acquisiti i dati della sessione dell'utente
	public EventCreated( Session activeSession ){
		this.activeSession = activeSession;
	}	
	
	
	// Viene mostrato l'ID dell'evento appena creato
	public void show_eventID(String EventID){
		
		// Se la sessione non � valida l'utente viene ritornato nella main page
		if(!activeSession.isValid()){
			Main NewMainIstance = new Main();
			NewMainIstance.onModuleLoad();
			return;
		}
		
		
		// Vengono eliminati dalla HTML_MAIN_CONTENT tutti gli eventuali widgets presenti
		Layout.clean_everything_in_html_main_content();		
		
		// Viene aggiunto il titolo della form
		Layout.add_title("Event successfully created");
		
		// Pannello orizzontale in cui verranno inserite due Label
		HorizontalPanel horizontalpanelMiddle = new HorizontalPanel();
		
		Label labelMessage = new Label("The ID for this event is ");
		Label labelEventID = new Label(EventID);
		
		horizontalpanelMiddle.setStyleName("css-eventcreate_horizontalpanelMiddle");
		labelMessage.setStyleName("css-eventcreate_label");
		labelEventID.setStyleName("css-eventcreate_id");
		
		// Si aggiungono le due Label al pannello orizzontale
		horizontalpanelMiddle.add(labelMessage);
		horizontalpanelMiddle.add(labelEventID);
		
		// Aggiunta del pannello orizzontale alla pagina per la visualizzazione
		RootPanel.get("html_main_content_middle").add(horizontalpanelMiddle);
	}
}
