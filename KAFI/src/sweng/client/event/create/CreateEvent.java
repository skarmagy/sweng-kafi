package sweng.client.event.create;

import sweng.client.Main;
import sweng.client.event.create.datebox.HandlerDateBox;
import sweng.shared.Layout;
import sweng.shared.Session;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DateBox;


/* Classe che gestisce la creazione di un evento, compreso il layout della form */
public class CreateEvent {
	
	// Dati della sessione dell'utente
	protected Session activeSession;
	
	// Pannello TOP in cui verranno inserite le textbox statiche
	protected VerticalPanel TopPanel;
	protected TextBox textboxEventName;
	protected TextBox textboxEventPlace;
	protected TextArea textareaEventDescription;
	protected Button buttonCreateEvent;
	
	// Pannello MIDDLE in cui verranno inserite le databox e le textbox dinamiche dei giorni dell'evento
	protected VerticalPanel MiddlePanel;
	
	// Pannello BOTTOM statico
	protected VerticalPanel BottomPanel;
	protected Label labelError;
	
	
	// Vengono acquisiti i dati della sessione dell'utente
	public CreateEvent(Session activeSession){
		this.activeSession = activeSession;
	}
	
	// Viene visualizzata la form di creazione dell'evento
	public void form(){

		// Se la sessione non � valida l'utente viene ritornato nella main page
		if(!activeSession.isValid()){
			Main NewMainIstance = new Main();
			NewMainIstance.onModuleLoad();
			return;
		}
		
		// Vengono eliminati dalla HTML_MAIN_CONTENT tutti gli eventuali widgets presenti
		Layout.clean_everything_in_html_main_content();		
		
		
		// Aggiungo il titolo alla form
		Layout.add_title("Create event");
		
		
		
		/* [TOP] Pannello Statico */
		TopPanel = new VerticalPanel();
		
		Label labelEventName = new Label("Title");
		TopPanel.add(labelEventName);
		labelEventName.setStyleName("css-generics_label");
		
		textboxEventName = new TextBox();
		TopPanel.add(textboxEventName);
		textboxEventName.setStyleName("css-generics_textbox");
		
		Label labelEventPlace = new Label("Place");
		TopPanel.add(labelEventPlace);
		labelEventPlace.setStyleName("css-generics_label");
		
		textboxEventPlace = new TextBox();
		TopPanel.add(textboxEventPlace);
		textboxEventPlace.setStyleName("css-generics_textbox");
		
		Label labelEventDescription = new Label("Description (optional)");
		TopPanel.add(labelEventDescription);
		labelEventDescription.setStyleName("css-generics_label");
		
		textareaEventDescription = new TextArea();
		TopPanel.add(textareaEventDescription);
		textareaEventDescription.setStyleName("css-generics_textbox");
		textareaEventDescription.setCharacterWidth(12);
		textareaEventDescription.setVisibleLines(1);
    	
    	
		// Inserisco il pannello statico nella pagina
		RootPanel.get("html_main_content_top").add(TopPanel);
		
		
		
		/* [MIDDLE] Pannello Dinamico */
		MiddlePanel = new VerticalPanel();
		
		
		// Bottone che aggiunger� una nuova DateBox
		Button buttonAddDateBox = new Button();
		buttonAddDateBox.setText("Add a date"); 
		MiddlePanel.add(buttonAddDateBox);
		buttonAddDateBox.setStyleName("css-createevent_button_add_event");
		

		// Handler che crea una nuova DateBox
		buttonAddDateBox.addClickHandler(new ClickHandler(){
			public void onClick(ClickEvent event) {
				
				HandlerDateBox handler = new HandlerDateBox();
				
				DateBox dateBox = new DateBox();
				
				dateBox = handler.CreateDateBox();
				
				// Elimina dal formato della data di DateBox le ore/minuti/secondi, visualizzando il restante
				dateBox.setFormat(new DateBox.DefaultFormat(DateTimeFormat.getFormat("yyyy/MMMM/dd")));

				
				// Pannello orizzontale in cui inseriro la label che conterr� il numero della data inserita
				final HorizontalPanel hpanelDateBox_DateNumber = new HorizontalPanel();
				// Pannello orizzontale in cui inseriro un buttone X che servir� per cancellare la DateBox creata, e la DateBox */
				final HorizontalPanel hpanelDateBox_DateBox = new HorizontalPanel();
				// Pannello orizzontale in cui inserir� la Label che ci conta il numero della data (esempio Date N� 1, Date N� 2, ..)
				final HorizontalPanel hpanelDateBox_HoursLabel = new HorizontalPanel();
				// Pannello orizzontale in cui inserir� le textbox per le date
				final HorizontalPanel hpanelDateBox_Hours = new HorizontalPanel();
			
				
				// Label che ci conta il numero della data (esempio Date N� 1, Date N� 2, ..)
				Label labelDateNumber = new Label();
				labelDateNumber.setText("Event Date");
				hpanelDateBox_DateNumber.add(labelDateNumber);
				labelDateNumber.setStyleName("css-generics_label");
				
				
				// Bottone per eliminare la DateBox
				PushButton button_deleteDateBox = new PushButton(new Image(GWT.getModuleBaseURL() + "gwt/clean/images/icon_TRASH.gif"));				
				hpanelDateBox_DateBox.add(button_deleteDateBox);
				
				hpanelDateBox_DateBox.add(dateBox);
				dateBox.getElement().setPropertyString("placeholder", "year/month/day");
				dateBox.setStyleName("css-createevent_textbox_date_hours");

				
				
				// Label "Specify the hours for this date"
				Label labelHoursLabel = new Label();
				labelHoursLabel.setText("Set the hours for this date");
				hpanelDateBox_HoursLabel.add(labelHoursLabel);
				labelHoursLabel.setStyleName("css-generics_label");
				
				// TextBoxes che conterranno gli orari dell'evento
				TextBox textboxHours = new TextBox();	
				PushButton buttonAddAnotherDate = new PushButton(new Image(GWT.getModuleBaseURL() + "gwt/clean/images/icon_ADD.png"));
				
				hpanelDateBox_Hours.add(buttonAddAnotherDate);
				hpanelDateBox_Hours.add(textboxHours);
				textboxHours.getElement().setPropertyString("placeholder", "00.00");
				textboxHours.setStyleName("css-createevent_textbox_date_hours");
				
	
				// Inserisco i panel nella MiddlePanel
				MiddlePanel.add(hpanelDateBox_DateNumber);
				MiddlePanel.add(hpanelDateBox_DateBox);
				MiddlePanel.add(hpanelDateBox_HoursLabel);
				MiddlePanel.add(hpanelDateBox_Hours);

				
				// Handler del click sul bottone che cancella la DateBox
				button_deleteDateBox.addClickHandler(new ClickHandler(){
					public void onClick(ClickEvent event) {
						MiddlePanel.remove(hpanelDateBox_DateNumber);
						MiddlePanel.remove(hpanelDateBox_DateBox);
						MiddlePanel.remove(hpanelDateBox_HoursLabel);
						MiddlePanel.remove(hpanelDateBox_Hours);
					}
				});
				
				// Handler del click sul bottone che aggiunge una TextBox che gestisce le ore dell'evento
				buttonAddAnotherDate.addClickHandler(new ClickHandler(){
					public void onClick(ClickEvent event) {
						TextBox textboxHours = new TextBox();
						hpanelDateBox_Hours.add(textboxHours);
						textboxHours.getElement().setPropertyString("placeholder", "00.00");
						textboxHours.setStyleName("css-createevent_textbox_date_hours");
					}
				});
			}
		});
	
		
		RootPanel.get("html_main_content_middle").add(MiddlePanel);
		
		// Genero un click per generare subito una DateBox
		buttonAddDateBox.click();
		
		/* [BOTTOM] Pannello Statico */
		BottomPanel = new VerticalPanel();
		
		// Label che visualizzer� eventuali errori nella compilazione della Form
		labelError = Layout.add_labelError();
		BottomPanel.add(labelError);
		
		
		// Bottone invier� la form al server
		buttonCreateEvent = new Button();
		buttonCreateEvent.setText("Create Event"); 
		BottomPanel.add(buttonCreateEvent);
		buttonCreateEvent.setStyleName("css-createevent_button_create_event");
		
		// Creo e gestisco l'evento Click sul bottone "Create Event"
		HandlerCreateEvent handler = new HandlerCreateEvent(this, activeSession);
		buttonCreateEvent.addClickHandler(handler);
		
		RootPanel.get("html_main_content_bottom").add(BottomPanel);
	}
}
