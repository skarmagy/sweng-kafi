package sweng.client.event.create.datebox;

import java.util.Date;

import com.google.gwt.event.logical.shared.ShowRangeEvent;
import com.google.gwt.event.logical.shared.ShowRangeHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.datepicker.client.DateBox;

/* Classe che gestisce la creazione di una DateBox dinamica nella CreateEvent */
public class HandlerDateBox {
	
	protected static Date d;	
	
	private static Date today()
	{
	    return zeroTime(new Date());
	}
	 
	/** this is important to get rid of the time portion, including ms */
	private static Date zeroTime(final Date date)
	{
		return DateTimeFormat.getFormat("yyyyMMdd").parse(DateTimeFormat.getFormat("yyyyMMdd").format(date));
	}
	 
	private static void nextDay(final Date d)
	{
		com.google.gwt.user.datepicker.client.CalendarUtil.addDaysToDate(d, 1);
	}
	
	
	public HandlerDateBox(){
	}
	
	
	/* Crea una DateBox nell'istanza Evento passata*/
	public DateBox CreateDateBox(){
		
		final DateBox dateBox = new DateBox(); 
		
		dateBox.addValueChangeHandler(new ValueChangeHandler<Date>()
		{
		    @Override
		    public void onValueChange(final ValueChangeEvent<Date> dateValueChangeEvent)
		    {
		        if (dateValueChangeEvent.getValue().before(today()))
		        {
		            dateBox.setValue(today(), false);
		        }
		    }
		});
		
		dateBox.getDatePicker().addShowRangeHandler(new ShowRangeHandler<Date>()
		{
		    @Override
		    public void onShowRange(final ShowRangeEvent<Date> dateShowRangeEvent)
		    {
		        final Date today = today();
		        Date d = zeroTime(dateShowRangeEvent.getStart());
		        while (d.before(today))
		        {
		            dateBox.getDatePicker().setTransientEnabledOnDates(false, d);
		            nextDay(d);
		        }
		    }
		});

		return dateBox;
	}
}
