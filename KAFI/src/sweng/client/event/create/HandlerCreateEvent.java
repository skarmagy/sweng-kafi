package sweng.client.event.create;

import java.util.LinkedList;

import sweng.client.Main;
import sweng.client.event.create.created.EventCreated;
import sweng.client.services.CreateEventService;
import sweng.client.services.CreateEventServiceAsync;
import sweng.shared.FieldVerifier;
import sweng.shared.POJO;
import sweng.shared.Session;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.VerticalPanel;


/* Classe che gestisce l'evento click sul bottone "Create Event" */
public class HandlerCreateEvent implements ClickHandler {

	// Dati della sessione dell'utente
	protected Session activeSession;
	
	// Instanza dell'evento in creazione
	protected CreateEvent Event;
	
	// Username dell'utente dopo aver effettuato il Login/Registrazione
	protected String username;
    
	// Pannello in cui ci sono le label, textbox statiche
	protected VerticalPanel TopPanel;
	// Pannello in cui ci sono le databox e le textbox dinamiche dei giorni dell'evento
	protected VerticalPanel MiddlePanel;
	// Pannello in cui c'e il bottone che crea l'evento
	protected VerticalPanel BottomPanel;
	
	
    
    // Acquisisce l'istanza CreateEvent per poter validare la sua form
    public HandlerCreateEvent(CreateEvent Event, Session activeSession)  {
        
    	this.activeSession = activeSession;
    	this.Event = Event;
    	this.TopPanel = Event.TopPanel;
    	this.MiddlePanel = Event.MiddlePanel;
    	this.BottomPanel = Event.BottomPanel;
    }
    
    
    // All'evento Click vengono estrapolati i dati dalla form CreateEvent, controllati e passati al server
	public void onClick(ClickEvent event) {	
		
		// Se la sessione non � valida l'utente viene ritornato nella main page
		if(!activeSession.isValid()){
			Main NewMainIstance = new Main();
			NewMainIstance.onModuleLoad();
			return;
		}
		
		String username = activeSession.getSession_username();
		
		// Vengono cancellati eventuali vecchi errori dalla labelError
		Event.labelError.setText("");
		
		LinkedList<LinkedList<String>> list = new LinkedList<LinkedList<String>>();
		
		// Ritorna una [lista di liste] con tutti gli elementi della form CreateEvent
		// Vengono fatti dei primi controlli sull'integrit� dei dati
		list = FieldVerifier.CreateEvent_validate_client_side(TopPanel, MiddlePanel, Event.labelError, username);
		
		// Se la lista ritornata � vuota allora qualche dato della form � errato
		// l'utente non pu� proseguire comunicando col server
		if( list.isEmpty()){
			return;
		}
		
	    // Disabilito il bottone "Create Event"
		Event.buttonCreateEvent.setEnabled(false);
		
		// Funzione che eseguir� la chiamata RPC che elaborer� i dati della form 
		createEventToServer(list);
	}
	
	
	// Invia i dati validati della form (l'EventName,EventPlace,..) al server per l'elaborazione
	private void createEventToServer(LinkedList<LinkedList<String>> arrayEvent_details) {
		
		final CreateEventServiceAsync createeventService = GWT.create(CreateEventService.class);
		
		createeventService.createeventServer(arrayEvent_details,
				new AsyncCallback<POJO>() {
					public void onFailure(Throwable caught) {
						
						// Impossibile stabilire una comunicazione RPC
						Event.labelError.setText("Fatal error!");
					}
					
					public void onSuccess(POJO pojoServerResponseObject) {

						// Il server ritorna un errore
						if(pojoServerResponseObject.getServerResponse() == 0){
							
							// Visualizzo l'errore
							Event.labelError.setText(pojoServerResponseObject.getServerResponseMessage());
							
							// Riattivazione bottone "Create Event"
							Event.buttonCreateEvent.setEnabled(true);
						}
						
						
						// Il server ha creato l'evento
						if(pojoServerResponseObject.getServerResponse() == 1){
							
							// ID dell'evento appena creato
							String EventID = pojoServerResponseObject.getServerResponseMessage();
							
							EventCreated EventCreatedIstance = new EventCreated(activeSession);
							
							// Viene mostrato l'ID dell'evento appena creato
							EventCreatedIstance.show_eventID(EventID);
						}
					}
				});
	}
}
