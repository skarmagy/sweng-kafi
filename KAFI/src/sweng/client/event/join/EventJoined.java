package sweng.client.event.join;

import java.util.LinkedList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.RootPanel;

import sweng.client.event.MainEvent;
import sweng.client.services.SearchEventService;
import sweng.client.services.SearchEventServiceAsync;
import sweng.shared.Layout;
import sweng.shared.POJO;
import sweng.shared.Session;

import com.google.gwt.user.client.ui.Label;


/* Classe che mostra a video una notifica che avvisa l'utente dell'avvenuta iscrizione all'evento */
public class EventJoined {
	
	protected Session activeSession;
	protected String EventID;
	
	public EventJoined(String EventID, Session activeSession){
		this.activeSession = activeSession;
		this.EventID = EventID;
	}

	public void show(){
		
		// Vengono eliminati dalla HTML_MAIN_CONTENT tutti gli eventuali widgets presenti
		Layout.clean_everything_in_html_main_content();		
		
		// Viene aggiunta la Label di errore della form
		final Label labelError = Layout.add_labelError();
		
		// Aggiungo il titolo alla form
		Layout.add_title("Success");
		
		HorizontalPanel hpPanel = new HorizontalPanel();
		
		Label labelText = new Label("You successfully joined the event.");		
		
		PushButton pushbuttonGoBackToTheEvent = new PushButton();
		pushbuttonGoBackToTheEvent.setText("Click here");
		pushbuttonGoBackToTheEvent.setStyleName("css-eventjoined_pushbutton_gobacktotheevent");
				
		pushbuttonGoBackToTheEvent.addClickHandler(new ClickHandler(){
			public void onClick(ClickEvent event) {
				
				final SearchEventServiceAsync searcheventService = GWT.create(SearchEventService.class);	 
				
				searcheventService.searcheventServer(EventID,
						new AsyncCallback<POJO>() {
							public void onFailure(Throwable caught) {
								
								// Impossibile stabilire una comunicazione RPC
								labelError.setText("Fatal error!");
							}
							
							public void onSuccess(POJO pojoServerResponseObject) {
								
								// L'evento non � stato trovato oppure � stato generato un errore con il database
								if(pojoServerResponseObject.getServerResponse() == 0){
									
									// Visualizzo l'errore
									labelError.setText(pojoServerResponseObject.getServerResponseMessage());
								}
								
								
								// L'evento � stato trovato
								if(pojoServerResponseObject.getServerResponse() == 1){
																
									LinkedList<LinkedList<String>> listEventDetails = pojoServerResponseObject.getServerResponseList();
									
									MainEvent MainEventIstance = new MainEvent(activeSession);
									MainEventIstance.callShowEvent(listEventDetails);
								}
							}
						});
			}
		});
		
		Label labelText2 = new Label("to return at the event.");	
		
		hpPanel.add(labelText);
		hpPanel.add(pushbuttonGoBackToTheEvent);
		hpPanel.add(labelText2);
		
		RootPanel.get("html_main_content_middle").add(hpPanel);
	}
}
