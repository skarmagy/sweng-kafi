package sweng.client.event.join;

import java.util.LinkedList;

import sweng.client.event.show.ShowEvent;
import sweng.client.services.JoinTheEventService;
import sweng.client.services.JoinTheEventServiceAsync;
import sweng.shared.POJO;
import sweng.shared.Session;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;

/* iscrive l'utente all'evento (con data e ora fissati) */
public class HandlerJoinTheEvent {
	
	protected Session activeSession;
	
	protected ShowEvent ShowEventIstance;
	protected String EventID;
	protected String Username;
	protected String Name;
	protected LinkedList<LinkedList<String>> TmpList;
	protected Label labelError;
	
	
	public HandlerJoinTheEvent(ShowEvent ShowEventIstance, Session activeSession){
		this.ShowEventIstance = ShowEventIstance;
		
		this.activeSession = activeSession;
				
		this.EventID = ShowEventIstance.EventID;
		this.Username = ShowEventIstance.Username;
		this.Name = ShowEventIstance.Name;
		
		// TmpList contiene tutte le date selezionate dall'utente su cui quest'ultimo si vuole unire
		this.TmpList = ShowEventIstance.TmpList;
		this.labelError = ShowEventIstance.labelError;
	}
	
	public void joinTheEvent(){
		
		TmpList.addFirst(new LinkedList<String>());
		LinkedList<String> TmpSubList = TmpList.get(0);
		TmpSubList.add(EventID);
		TmpSubList.add(Username);
		TmpSubList.add(Name);
		
		final JoinTheEventServiceAsync join_the_eventService = GWT.create(JoinTheEventService.class);	 
		
		join_the_eventService.join_the_eventServer(TmpList,
				new AsyncCallback<POJO>() {
					public void onFailure(Throwable caught) {
						
						// Impossibile stabilire una comunicazione RPC
						labelError.setText("Fatal error!");
					}
					
					public void onSuccess(POJO pojoServerResponseObject) {
						
						// L'evento non � stato trovato oppure � stato generato un errore con il database
						if(pojoServerResponseObject.getServerResponse() == 0){
							
							// Visualizzo l'errore
							labelError.setText(pojoServerResponseObject.getServerResponseMessage());
						}
						
						
						// L'evento � stato trovato
						if(pojoServerResponseObject.getServerResponse() == 1){
							
							EventJoined EventJoinedIstance = new EventJoined(EventID, activeSession);
							EventJoinedIstance.show();
						}
					}
				});
	}

}

