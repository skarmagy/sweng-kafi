package sweng.client.event.show;

import java.util.Iterator;
import java.util.LinkedList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import sweng.client.event.join.HandlerJoinTheEvent;
import sweng.client.popup.PopupMain;
import sweng.client.services.CheckIfUserIsPartecipantService;
import sweng.client.services.CheckIfUserIsPartecipantServiceAsync;
import sweng.client.services.PartecipantService;
import sweng.client.services.PartecipantServiceAsync;
import sweng.shared.FieldVerifier;
import sweng.shared.Layout;
import sweng.shared.POJO;
import sweng.shared.Session;

/* Viene stampato a video l'evento, con tutti i dettagli relativi ad esso */
public class ShowEvent {

	// Dati della sessione dell'utente
	protected Session activeSession; 
	
	public String EventID;
	public String Username;
	public String Name;
	
	public TextBox textboxName;
	public Label labelError;
	
	public LinkedList<LinkedList<String>> TmpList;
	
	// Vengono acquisiti i dati della sessione dell'utente
	public ShowEvent(Session activeSession){
		
		this.activeSession = activeSession;
		this.TmpList = null;
		this.Username = null;
		this.Name = null;
	}
	
	public void stamp_event(LinkedList<LinkedList<String>> list){
		
		final ShowEvent ShowEventIstance = this;
		
		// Vengono eliminati dalla HTML_MAIN_CONTENT tutti gli eventuali widgets presenti
		Layout.clean_everything_in_html_main_content();	
		
		
		EventID = list.get(0).get(0);
		Username = activeSession.getSession_username();
		String EventName = list.get(0).get(1);
		String EventPlace = list.get(0).get(2);
		String EventDescription = list.get(0).get(3);
		String EventOwner = list.get(0).get(4);
		String EventAvailability = list.get(0).get(5);
		String EventNote = list.get(0).get(6);
		boolean Event_isOpen = false;
		
		if(EventAvailability.compareTo("1") == 0){
			EventAvailability = "Event open";
			Event_isOpen = true;
		}
		else{
			EventAvailability = "Event closed";
			Event_isOpen = false;
		}
		
		if(EventDescription.compareTo("") == 0){
			EventDescription = "No description available";
		}
		
		
		// Aggiungo il titolo alla form
		Layout.add_title("Event details");
		
		
		VerticalPanel vpContentPanel = new VerticalPanel();
		
			VerticalPanel vpContentPanel_top = new VerticalPanel();
			
				if(EventNote != null){
					
					HorizontalPanel hpNote = new HorizontalPanel();
					
						Image imageNote = new Image( GWT.getModuleBaseURL() + "gwt/clean/images/note.png"); 
						Label labelNoteText = new Label(EventNote);
						
						hpNote.add(imageNote);
						hpNote.add(labelNoteText);
				
					
					hpNote.setStyleName("css-showevent_hpnote");
					vpContentPanel_top.add(hpNote);	
					
				}
				
				HorizontalPanel hpEventName = new HorizontalPanel();
				
					Label labelTitleEventName = new Label("Name");
					Label labelEventName = new Label(EventName);
					
					labelTitleEventName.setStyleName("css-showevent_content_title");
					labelEventName.setStyleName("css-showevent_content");
					
					hpEventName.add(labelTitleEventName);
					hpEventName.add(labelEventName);
					
				HorizontalPanel hpEventPlace = new HorizontalPanel();	
					
					Label labelTitleEventPlace = new Label("Place");
					Label labelEventPlace = new Label(EventPlace);
					
					labelTitleEventPlace.setStyleName("css-showevent_content_title");
					labelEventPlace.setStyleName("css-showevent_content");
					
					hpEventPlace.add(labelTitleEventPlace);
					hpEventPlace.add(labelEventPlace);
					
				HorizontalPanel hpEventDescription = new HorizontalPanel();	
					
					Label labelTitleEventDescription = new Label("Description");
					Label labelEventDescription = new Label(EventDescription);
					
					labelTitleEventDescription.setStyleName("css-showevent_content_title");
					labelEventDescription.setStyleName("css-showevent_content");
					
					hpEventDescription.add(labelTitleEventDescription);
					hpEventDescription.add(labelEventDescription);
					
				
			vpContentPanel_top.add(hpEventName);
			vpContentPanel_top.add(hpEventPlace);
			vpContentPanel_top.add(hpEventDescription);
			
			
			VerticalPanel vpContentPanel_middle = new VerticalPanel();
			
				HorizontalPanel hpEventID = new HorizontalPanel();
				
					Label labelTitleEventID = new Label("ID");
					Label labelEventID = new Label(EventID);
					
					labelTitleEventID.setStyleName("css-showevent_content_title");
					labelEventID.setStyleName("css-showevent_content");
					
					hpEventID.add(labelTitleEventID);
					hpEventID.add(labelEventID);
			
				HorizontalPanel hpEventOwner = new HorizontalPanel();
				
					Label labelTitleEventOwner = new Label("Created by");
					Label labelEventOwner = new Label(EventOwner);
					
					labelTitleEventOwner.setStyleName("css-showevent_content_title");
					labelEventOwner.setStyleName("css-showevent_content");
					
					hpEventOwner.add(labelTitleEventOwner);
					hpEventOwner.add(labelEventOwner);
					
				HorizontalPanel hpEventIsOpen = new HorizontalPanel();
				
					Label labelTitleEventIsOpen = new Label("Availability");
					Label labelEventIsOpen = new Label(EventAvailability);
				
					labelTitleEventIsOpen.setStyleName("css-showevent_content_title");
					labelEventIsOpen.setStyleName("css-showevent_content");
				
					hpEventIsOpen.add(labelTitleEventIsOpen);
					hpEventIsOpen.add(labelEventIsOpen);
					

					
				HorizontalPanel hpComments = new HorizontalPanel();
					
					PushButton pushbuttonComments = new PushButton();
					
					pushbuttonComments.setText("SHOW COMMENTS");
					pushbuttonComments.setStyleName("css-showevent_pushbuttonComment");
					
					pushbuttonComments.addClickHandler(new ClickHandler(){
						public void onClick(ClickEvent event) {
							
							PopupMain.createPopupComments(activeSession, EventID);
						}
					});
					
					hpComments.add(pushbuttonComments);
					
					
				vpContentPanel_middle.add(hpEventID);
				vpContentPanel_middle.add(hpEventOwner);
				vpContentPanel_middle.add(hpEventIsOpen);
				vpContentPanel_middle.add(hpComments);
			
						
		
		// Contatore che scorre la prima lista, parte da 1 perch� in posizione 0 non ci sono le date/orari
		int list_cont = 1;		
		// Contatore che scorre la seconda lista, parte da 1 perch� in posizione 0 non ci sono le date/orari
		int list_list_cont = 1;
		
		// Pannello che conterr� tutte le date e tutti gli orari
		VerticalPanel vpContentPanel_bottom = new VerticalPanel();
		
		do{
			
			VerticalPanel vpContentPanel_bottom_container = new VerticalPanel();
				list_list_cont = 1;
				
				HorizontalPanel hpEventDate = new HorizontalPanel();
				
					Label labelEventDate = new Label(list.get(list_cont).get(0));
					labelEventDate.setStyleName("css-showevent_form_title");
					hpEventDate.add(labelEventDate);
					
				HorizontalPanel hpData  = new HorizontalPanel();
				
					Label labelSubscribers = new Label();
					labelSubscribers.setText("Partecipants n.");
					labelSubscribers.setStyleName("css-showevent_form_title");
					hpData.add(labelSubscribers);
					
				HorizontalPanel hpCheckbox  = new HorizontalPanel();
				
					Label labelEmpty = new Label();
					labelEmpty.setStyleName("css-showevent_form_title_empty");
					hpCheckbox.add(labelEmpty);
					
				do{		
					
					HorizontalPanel hpEventHour_container  = new HorizontalPanel();
					
						Label labelEventHour = new Label(list.get(list_cont).get(list_list_cont));
						labelEventHour.setStyleName("css-showevent_middle_text");
						
						hpEventHour_container.add(labelEventHour);
						hpEventHour_container.setStyleName("css-showevent_form_container_normal");
						
						hpEventDate.add(hpEventHour_container);
	
						
					HorizontalPanel hpEventPartecipant_container  = new HorizontalPanel();	
						
						PushButton pushbuttonPartecipantsNumber = new PushButton();
					
						final String date = list.get(list_cont).get(0);
						final String hour = list.get(list_cont).get(list_list_cont);
						
						pushbuttonPartecipantsNumber.addClickHandler(new ClickHandler(){
							public void onClick(ClickEvent event) {
								
								PopupMain.createPopupShowPartecipants(EventID, date, hour);
							}
						});
						
						getPartecipantsNumber(pushbuttonPartecipantsNumber, EventID, list.get(list_cont).get(0), list.get(list_cont).get(list_list_cont));
						
						hpEventPartecipant_container.add(pushbuttonPartecipantsNumber);
						pushbuttonPartecipantsNumber.setStyleName("css-showevent_form_pushbuttonPartecipantsNumber");
	
						hpEventPartecipant_container.setStyleName("css-showevent_form_container_normal");
						
						hpData.add(hpEventPartecipant_container);

					
					HorizontalPanel hpCheckbox_container  = new HorizontalPanel();
					
						final CheckBox Checkbox = new CheckBox();
						Checkbox.setValue(false);
						Checkbox.setStyleName("css-showevent_middle_checkbox");
							
						hpCheckbox_container.setStyleName("css-showevent_form_container_normal");
						
						if(activeSession.isValid())
							check_ifUserAlreadySignedIn_forThisDate(date, hour, Checkbox, hpCheckbox_container);

						
						if(Event_isOpen)
							hpCheckbox_container.add(Checkbox);
					
						hpCheckbox.add(hpCheckbox_container);
						
						Checkbox.addClickHandler(new ClickHandler(){
							public void onClick(ClickEvent event) {
								
								createTemporaryList_userSelectedEvents(Checkbox.getValue(), date, hour);
							}
						});
						
						
					list_list_cont++;
						
				}while(list_list_cont != list.get(list_cont).size());
				
				vpContentPanel_bottom_container.add(hpEventDate);
				vpContentPanel_bottom_container.add(hpData);
				vpContentPanel_bottom_container.add(hpCheckbox);
				
				vpContentPanel_bottom_container.setStyleName("css-showevent_form_container_padding");
				vpContentPanel_bottom.add(vpContentPanel_bottom_container);
				
				
			list_cont++;
			
		}while(list_cont != list.size());
		
		
		vpContentPanel.add(vpContentPanel_top);
		vpContentPanel.add(vpContentPanel_middle);
		vpContentPanel.add(vpContentPanel_bottom);
		
		vpContentPanel_middle.setStyleName("css-showevent_verticalpanel_padding");
		vpContentPanel_bottom.setStyleName("css-showevent_verticalpanel_bottom_padding");
		
		RootPanel.get("html_main_content_middle").add(vpContentPanel);
		
		
		if(Event_isOpen){
			
			HorizontalPanel hpBottom = new HorizontalPanel();
			
				if(activeSession.getSession_username() == null){
					textboxName = new TextBox();
					textboxName.getElement().setPropertyString("placeholder", "Your name");
					textboxName.setStyleName("css-showevent_textboxName");
					
					hpBottom.add(textboxName);
				}
				
				
				HorizontalPanel hpBottom_buttonContainer = new HorizontalPanel();
			
					PushButton pushbuttonJoinTheEvent = new PushButton();
					pushbuttonJoinTheEvent.setText("JOIN THE EVENT");
					pushbuttonJoinTheEvent.setStyleName("css-showevent_buttonJoinMyEvent");
		
					pushbuttonJoinTheEvent.addClickHandler(new ClickHandler(){
										public void onClick(ClickEvent event) {
											
											if(TmpList == null){
												labelError.setText("You must select at least one event to be joined.");
												return;
											}
											
											if(activeSession.getSession_username() == null){
												Username = null;
												Name = textboxName.getText();
												
												String result_NameValidation = FieldVerifier.validateStringFormat(Name, 3, 20);
												
												if(result_NameValidation != "ok"){
													labelError.setText(result_NameValidation);
													return;
												}
												
											} else{
												Username = activeSession.getSession_username();
												Name = activeSession.getSession_name();
											}
											
											HandlerJoinTheEvent JoinTheEventIstance = new HandlerJoinTheEvent(ShowEventIstance, activeSession);
											JoinTheEventIstance.joinTheEvent();
										}
									});
					
					hpBottom_buttonContainer.add(pushbuttonJoinTheEvent);
					hpBottom_buttonContainer.setStyleName("css-showevent_hpBottom_buttonContainer");
				
			hpBottom.add(hpBottom_buttonContainer);

			RootPanel.get("html_main_content_bottom").add(hpBottom);
		}
		
		// Viene aggiunta la Label di errore della form
		labelError = Layout.add_labelError();
	}
	
	// Controlla se l'utente per quella data si � gi� iscritto, disabilitando la checkbox di iscrizione
	private void check_ifUserAlreadySignedIn_forThisDate(String Date, String Hour, final CheckBox Checkbox, final HorizontalPanel hpCheckbox_container) {
		
		String[] data = new String[4];
		data[0] = EventID;
		data[1] = Username;
		data[2] = Date;
		data[3] = Hour;
		
		CheckIfUserIsPartecipantServiceAsync check_if_user_is_partecipantService = GWT.create(CheckIfUserIsPartecipantService.class);	 
		
		check_if_user_is_partecipantService.check_if_user_is_partecipantServer(data,
				new AsyncCallback<POJO>() {
					public void onFailure(Throwable caught) {
						
						// Impossibile stabilire una comunicazione RPC
						labelError.setText("Fatal error!");
					}
					
					public void onSuccess(POJO pojoServerResponseObject) {
						
						// L'evento non � stato trovato oppure � stato generato un errore con il database
						if(pojoServerResponseObject.getServerResponse() == 0){
							
							Checkbox.setVisible(false);
							
							Label labelText = new Label("JOINED");
							
							labelText.addStyleName("css-showevent_form_container_joined-text");
							hpCheckbox_container.addStyleName("css-showevent_form_container_green");
							
							hpCheckbox_container.add(labelText);
						}
					}
				});
		
		
	}
	
	// Scrive il numero dei partecipanti attuali ad un evento 
	private void getPartecipantsNumber(final PushButton pushbuttonPartecipantsNumber, final String id, final String date, final String hour){
		
		String[] data = new String[3];
		data[0] = id;
		data[1] = date;
		data[2] = hour;

		final PartecipantServiceAsync partecipantService = GWT.create(PartecipantService.class);	 
		
		partecipantService.partecipantServer(data,
				new AsyncCallback<POJO>() {
					public void onFailure(Throwable caught) {
						
						// Impossibile stabilire una comunicazione RPC
						labelError.setText("Fatal error!");
					}
					
					public void onSuccess(POJO pojoServerResponseObject) {
						
						// L'evento non � stato trovato oppure � stato generato un errore con il database
						if(pojoServerResponseObject.getServerResponse() == 0){
							
							// Visualizzo l'errore
							labelError.setText(pojoServerResponseObject.getServerResponseMessage());
						}
						
						
						// L'evento � stato trovato
						if(pojoServerResponseObject.getServerResponse() == 1){
							
							pushbuttonPartecipantsNumber.setText(pojoServerResponseObject.getServerResponseMessage());
						}
					}
				});
	}
	
	// Crea una lista temporanea con le selezioni dell'utente sulle checkboxes, necessaria
	// quando l'utente si vorr� unire ai determinati eventi (HandlerJoinEvent)
	private void createTemporaryList_userSelectedEvents(boolean CheckBox_isChecked, String Date, String Hour){
		
		if(CheckBox_isChecked == false){
			
			// Creo un iteratore alla lista principale che servir� per scorrere gli elementi della lista
			Iterator<LinkedList<String>> List_iter = TmpList.listIterator();
			
			// Punto l'iteratore della lista principale alla prima lista
			LinkedList<String> SubList = List_iter.next();
			
			// Creo un iteratore alla lista figlia puntata dall'iteratore principale
			Iterator<String> SubList_iter = SubList.listIterator();
			
			boolean first_iteraction = true;
			
			do{
				if(!first_iteraction){
					SubList = List_iter.next();
					SubList_iter = SubList.listIterator();
				}
			
				
				if(SubList_iter.next() == Date && SubList_iter.next() == Hour){
					List_iter.remove();
					System.out.println(TmpList);
					return;
				}
				
				first_iteraction = false;
				
			}while(List_iter.hasNext());
			
		}
		
		if(CheckBox_isChecked == true){
			
			if(TmpList == null ){
				TmpList = new LinkedList<LinkedList<String>>();
				
				// INSERIMENTO
				LinkedList<String> TmpSubList = new LinkedList<String>();
				
				TmpList.add(new LinkedList<String>());
				TmpSubList = TmpList.getLast();
				
				TmpSubList.add(Date);
				TmpSubList.add(Hour);
				
				System.out.println(TmpList);
				return;				
			}			
		
			// INSERIMENTO
			LinkedList<String> TmpSubList = new LinkedList<String>();
			
			TmpList.add(new LinkedList<String>());
			TmpSubList = TmpList.getLast();
			
			TmpSubList.add(Date);
			TmpSubList.add(Hour);
			
			System.out.println(TmpList);
		}
	}
	
}
