package sweng.client.event.myevents;

import java.util.LinkedList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;

import sweng.client.Main;
import sweng.client.event.myevents.stamp.StampMyEvents;
import sweng.client.services.MyEventsService;
import sweng.client.services.MyEventsServiceAsync;
import sweng.shared.Layout;
import sweng.shared.POJO;
import sweng.shared.Session;

/* Classe che visualizza gli eventuali eventi creati dall'utente */
public class MyEvents{
	
	// Dati della sessione dell'utente
	protected Session activeSession;
	
	// Vengono acquisiti i dati della sessione dell'utente
	public MyEvents(Session activeSession){
		this.activeSession = activeSession;
	}
	
	// Viene mostrata la lista degli eventuali eventi creati dall'utente
	public void show_my_events_list(){
		
		// Se la sessione non � valida l'utente viene ritornato nella main page
		if(!activeSession.isValid()){
			Main NewMainIstance = new Main();
			NewMainIstance.onModuleLoad();
			return;
		}
		
		// Vengono eliminati dalla HTML_MENU_BAR tutti gli eventuali widgets presenti
		Layout.clean_everything_in_html_menu_bar();
		
		// Vengono eliminati dalla HTML_MAIN_CONTENT tutti gli eventuali widgets presenti
		Layout.clean_everything_in_html_main_content();		
		
		// Viene aggiunto il bottone "CREATE EVENT" nella HTML_MENU_BAR
		Layout.add_html_pushbuttonCreateEvent(activeSession);
		
		// Viene aggiunto il bottone "SEARCH EVENT" nella HTML_MENU_BAR
		Layout.add_html_pushbuttonSearchEvent(activeSession);
		
		// Viene aggiunto il bottone "MY EVENTS" nella HTML_MENU_BAR
		Layout.add_html_pushbuttonMyEvents(activeSession);
		
		// Viene aggiunto il titolo della form
		Layout.add_title("My Events");
		
		// Viene aggiunta la Label di errore della form
		Label labelError = Layout.add_labelError();

		// Funzione che eseguir� la chiamata RPC
		// che stamper� nella form un eventuale lista di eventi creati dall'utente
		getEventsFromServer(activeSession, labelError);
	}
	
	
	// Chiamata RPC che stamper� nella form un eventuale lista di eventi creati dall'utente
	private void getEventsFromServer(final Session activeSession, final Label labelError) {
		
		String username = activeSession.getSession_username();
		
		final MyEventsServiceAsync myeventsService = GWT.create(MyEventsService.class);	
		
		myeventsService.myeventsServer(username, new AsyncCallback<POJO>() {
					public void onFailure(Throwable caught) {
						
						// Impossibile stabilire una comunicazione RPC
						labelError.setText("Fatal error!");
					}
					
				
					public void onSuccess(POJO pojoServerResponseObject) {
						
						// Se nessun evento � stato creato dall'utente o � avvenuto un errore
						if(pojoServerResponseObject.getServerResponse() == 0){
							
							String error_message = pojoServerResponseObject.getServerResponseMessage();
							
							// Viene visualizzato l'errore nella labelError						
							labelError.setText(error_message);
							
							return;
						}
						
						
						// Il server restituisce una [lista di liste] di eventi
						if(pojoServerResponseObject.getServerResponse() == 1){
				
							LinkedList<LinkedList<String>> MyEvents_list = pojoServerResponseObject.getServerResponseList();
							
							StampMyEvents StampMyEventsIstance = new StampMyEvents(activeSession);
							
							// Viene visualizzato un layout di stampa per la lista degli eventi
							StampMyEventsIstance.stampEvents(MyEvents_list);
						}
					}
				});
	}

}
