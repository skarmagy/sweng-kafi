package sweng.client.event.myevents.stamp.delete;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;

import sweng.client.Main;
import sweng.client.event.myevents.MyEvents;
import sweng.client.event.myevents.stamp.StampMyEvents;
import sweng.client.services.DeleteEventService;
import sweng.client.services.DeleteEventServiceAsync;
import sweng.shared.POJO;
import sweng.shared.Session;


/* Classe che gestisce l'eliminazione di un evento */
public class HandlerDeleteEvent implements ClickHandler{
	
	// Dati della sessione dell'utente
	protected Session activeSession;
	
	protected StampMyEvents Event;
	protected String EventID;
	
	public HandlerDeleteEvent(StampMyEvents Event, String EventID, Session activeSession){
		this.activeSession = activeSession;
		this.Event = Event;
		this.EventID = EventID;
	}
	
	public void onClick(ClickEvent event) {
		
		// Se la sessione non � valida l'utente viene ritornato nella main page
		if(!activeSession.isValid()){
			Main NewMainIstance = new Main();
			NewMainIstance.onModuleLoad();
			return;
		}
		
		// Funzione che eseguir� la chiamata RPC per eliminare l'evento
		deleteeventToServer(EventID);	
	}
	
	// Invia l'EventID al server per cancellare l'evento associato
	private void deleteeventToServer( String EventID ) {
		
		final DeleteEventServiceAsync deleteeventService = GWT.create(DeleteEventService.class);
	
		deleteeventService.deleteeventServer(EventID,
				new AsyncCallback<POJO>() {
					public void onFailure(Throwable caught) {
						
						// Impossibile stabilire una comunicazione RPC
						Event.labelError.setText("Fatal error!");
					}

					public void onSuccess(POJO pojoServerResponseObject) {
						
						// Se l'eliminazione dell'evento non va a buon fine
						if(pojoServerResponseObject.getServerResponse() == 0){
							
							// Visualizzo l'errore
							Event.labelError.setText(pojoServerResponseObject.getServerResponseMessage());
						}
						
						// L'eliminazione dell'evento � andata a buon fine
						if(pojoServerResponseObject.getServerResponse() == 1){
							
							MyEvents MyEventsIstance = new MyEvents(activeSession);
							
							// Mostra a video la nuova lista di eventi aggiornata
							MyEventsIstance.show_my_events_list();;
						}
					}
				});
	}
}
