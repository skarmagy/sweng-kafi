package sweng.client.event.myevents.stamp;

import java.util.Iterator;
import java.util.LinkedList;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.RootPanel;

import sweng.client.Main;
import sweng.client.event.myevents.stamp.delete.HandlerDeleteEvent;
import sweng.client.popup.PopupMain;
import sweng.shared.Layout;
import sweng.shared.Session;

/* Classe che stampa nella pagina MyEvents la lista dei miei eventi */
public class StampMyEvents {
	
	// Dati della sessione dell'utente
	protected Session activeSession;
	
	public Label labelError;
	
	// Vengono acquisiti i dati della sessione dell'utente 
	public StampMyEvents(Session activeSession){
		this.activeSession = activeSession;	
	}
	
	// Data una [Lista di liste] di eventi, vengono stampati a video gli eventi dell'utente
	public void stampEvents(LinkedList<LinkedList<String>> listMyEvents){
		
		// Se la sessione non � valida l'utente viene ritornato nella main page
		if(!activeSession.isValid()){
			Main NewMainIstance = new Main();
			NewMainIstance.onModuleLoad();
			return;
		}
		
		// Vengono eliminati eventuali widgets presenti
		Layout.clean_html_main_content_middle();	
		Layout.clean_html_main_content_bottom();
		
		
		// Creo un iteratore alla lista principale che servir� per scorrere gli elementi della lista
		Iterator<LinkedList<String>> MainList_iter = listMyEvents.listIterator();
		
		// Punto l'iteratore della lista principale alla prima lista
		LinkedList<String> DinamicList = MainList_iter.next();
		
		// Creo un iteratore alla lista figlia puntata dall'iteratore principale
		Iterator<String> SubList_iter = DinamicList.listIterator();
		
		
		HorizontalPanel horizontalpanelStatic = new HorizontalPanel();
		
		Label labelTitleID = new Label("Event ID");	
		Label labelTitleEventName = new Label("Event Name");	
		Label labelTitleIsOpen = new Label("Availability");
		Label labelTitleCloseEvent = new Label("Close Event");
		Label labelTitleDeleteEvent = new Label("Delete Event");
		
		labelTitleID.setStyleName("css-myevents_title_small");
		labelTitleEventName.setStyleName("css-myevents_title");
		labelTitleIsOpen.setStyleName("css-myevents_title_small");
		labelTitleCloseEvent.setStyleName("css-myevents_title");
		labelTitleDeleteEvent.setStyleName("css-myevents_title");
		
		horizontalpanelStatic.add(labelTitleID);
		horizontalpanelStatic.add(labelTitleEventName);
		horizontalpanelStatic.add(labelTitleIsOpen);
		horizontalpanelStatic.add(labelTitleCloseEvent);
		horizontalpanelStatic.add(labelTitleDeleteEvent);
		
		RootPanel.get("html_main_content_middle").add(horizontalpanelStatic);
		
		// Label di errore
		labelError = Layout.add_labelError();
		
		
		Label labelID;
		Label labelEventName;
		Label labelIsOpen;
		PushButton pushbuttonCloseEvent;
		PushButton pushbuttonDeleteEvent;
		
		int cont = 0;
		
		HorizontalPanel horizontalpanelDynamic;
		
		do{
			if (cont != 0){
				DinamicList = MainList_iter.next();
				SubList_iter = DinamicList.listIterator();
			}
			
			horizontalpanelDynamic = new HorizontalPanel();
			
			labelID = new Label(SubList_iter.next());
			labelEventName = new Label(SubList_iter.next());
			
			// Se l'evento � aperto abilito la possibilit� di CHIUDERE e CANCELLARE l'evento
			if(SubList_iter.next().compareTo("1") == 0){
				labelIsOpen = new Label("Open");
				
				pushbuttonCloseEvent = new PushButton();
				pushbuttonCloseEvent.setText("Close");
				
				pushbuttonDeleteEvent = new PushButton();
				pushbuttonDeleteEvent.setText("Delete");
				
				final String EventID = labelID.getText();
				
				pushbuttonCloseEvent.addClickHandler(new ClickHandler(){
					public void onClick(ClickEvent event) {
						
						PopupMain.createPopupNote(activeSession, EventID);
					}
				});
				
				HandlerDeleteEvent handlerDeleteEvent = new HandlerDeleteEvent(this, labelID.getText(), activeSession);
				pushbuttonDeleteEvent.addClickHandler(handlerDeleteEvent);
				
				
				labelID.setStyleName("css-myevents_content_small");
				labelEventName.setStyleName("css-myevents_content");
				labelIsOpen.setStyleName("css-myevents_content_small");
				pushbuttonCloseEvent.setStyleName("css-myevents_content_button_close_event");
				pushbuttonDeleteEvent.setStyleName("css-myevents_content_button_delete_event");
				
				horizontalpanelDynamic.add(labelID);
				horizontalpanelDynamic.add(labelEventName);
				horizontalpanelDynamic.add(labelIsOpen);
				horizontalpanelDynamic.add(pushbuttonCloseEvent);
				horizontalpanelDynamic.add(pushbuttonDeleteEvent);

				RootPanel.get("html_main_content_middle").add(horizontalpanelDynamic);
			}else{
				// Se l'evento non � aperto abilito solo la possibilit� di CANCELLARE l'evento
				labelIsOpen = new Label("Closed");
				
				Label labelEmptyCloseButton = new Label();
				
				pushbuttonDeleteEvent = new PushButton();
				pushbuttonDeleteEvent.setText("Delete");
				
				HandlerDeleteEvent handlerDeleteEvent = new HandlerDeleteEvent(this, labelID.getText(), activeSession);
				pushbuttonDeleteEvent.addClickHandler(handlerDeleteEvent);
				
				
				labelID.setStyleName("css-myevents_content_small");
				labelEventName.setStyleName("css-myevents_content");
				labelIsOpen.setStyleName("css-myevents_content_small");
				labelEmptyCloseButton.setStyleName("css-myevents_content_empty");
				pushbuttonDeleteEvent.setStyleName("css-myevents_content_button_delete_event");
				
				horizontalpanelDynamic.add(labelID);
				horizontalpanelDynamic.add(labelEventName);
				horizontalpanelDynamic.add(labelIsOpen);
				horizontalpanelDynamic.add(labelEmptyCloseButton);
				horizontalpanelDynamic.add(pushbuttonDeleteEvent);
				
				RootPanel.get("html_main_content_middle").add(horizontalpanelDynamic);
			}
			
			cont++;
			
		}while(MainList_iter.hasNext());
	}

}
