package sweng.client.event.myevents.stamp.close;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;

import sweng.client.Main;
import sweng.client.event.myevents.MyEvents;
import sweng.client.popup.note.PopupNote;
import sweng.client.services.CloseEventService;
import sweng.client.services.CloseEventServiceAsync;
import sweng.shared.FieldVerifier;
import sweng.shared.POJO;
import sweng.shared.Session;


/* Classe che gestisce la chiusura di un evento */
public class HandlerCloseEvent implements ClickHandler{
	
	// Dati della sessione dell'utente
	protected Session activeSession;
	
	protected PopupNote popup;
	protected String EventID;
	protected boolean Note_isSet;
	
	public HandlerCloseEvent(Session activeSession, String EventID, PopupNote popup, boolean Note_isSet){
		this.popup = popup;
		this.EventID = EventID;
		this.activeSession = activeSession;
		this.Note_isSet = Note_isSet;		
	}
	
	public void onClick(ClickEvent event) {
		
		// Se la sessione non � valida l'utente viene ritornato nella main page
		if(!activeSession.isValid()){
			Main NewMainIstance = new Main();
			NewMainIstance.onModuleLoad();
			return;
		}
		
		popup.pushbuttonCreateNote.setEnabled(false);
		popup.pushbuttonSkip.setEnabled(false);
		
		String Note = null;
		
		if(Note_isSet){
			Note = popup.textareaNote.getText();
			
			String result_ofNoteValidation = FieldVerifier.validateStringFormat(Note, 5, 80);
			
			if( result_ofNoteValidation != "ok"){
				popup.labelError.setText("Note " + result_ofNoteValidation);
				
				popup.pushbuttonCreateNote.setEnabled(true);
				popup.pushbuttonSkip.setEnabled(true);
				
				return;
			}
			closeeventToServer(EventID, Note);
		} else {
			closeeventToServer(EventID, Note);
		}
	}
	
	// Invia l'EventID al server per chiudere l'evento associato
	private void closeeventToServer(String EventID, String Note){
		
		String[] data = new String[2];
		data[0] = EventID;
		data[1] = Note;
		
		final CloseEventServiceAsync closeeventService = GWT.create(CloseEventService.class);
	
		closeeventService.closeeventServer(data,
				new AsyncCallback<POJO>() {
					public void onFailure(Throwable caught) {
						
						// Impossibile stabilire una comunicazione RPC
						popup.labelError.setText("Fatal error!");
					}

					public void onSuccess(POJO pojoServerResponseObject) {
						
						// Se la chiusura dell'evento non va a buon fine
						if(pojoServerResponseObject.getServerResponse() == 0){
							
							// Visualizzo l'errore
							popup.labelError.setText(pojoServerResponseObject.getServerResponseMessage());
							
							popup.pushbuttonCreateNote.setEnabled(true);
							popup.pushbuttonSkip.setEnabled(true);
						}
						
						// La chiusura dell'evento � andata a buon fine
						if(pojoServerResponseObject.getServerResponse() == 1){
							
							// Chiude automaticamente il Popup Note aperto
							popup.hide();
							
							MyEvents MyEventsIstance = new MyEvents(activeSession);
							
							// Mostra a video la nuova lista di eventi aggiornata
							MyEventsIstance.show_my_events_list();;
						}
					}
				});
	}
}
