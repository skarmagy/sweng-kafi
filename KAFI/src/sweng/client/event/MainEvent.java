package sweng.client.event;

import java.util.LinkedList;

import sweng.client.Main;
import sweng.client.event.myevents.MyEvents;
import sweng.client.event.show.ShowEvent;
import sweng.shared.Layout;
import sweng.shared.Session;


/* Classe Main che gestisce tutti gli eventi di un utente appena loggato/registrato */
public class MainEvent {
	
	// Dati della sessione dell'utente
	protected Session activeSession; 
	
	// Vengono acquisiti i dati della sessione dell'utente
	public MainEvent(Session activeSession){
		this.activeSession = activeSession;
	}
	
	// Chiama la classe MyEvents, che stamper� la lista degli eventi creati dall'utente
	public void callMyEvents(){
		
		// Se la sessione non � valida l'utente viene ritornato nella main page
		if(!activeSession.isValid()){
			Main NewMainIstance = new Main();
			NewMainIstance.onModuleLoad();
			return;
		}
		
		// Viene aggiunto il bottone "LOGOUT" nella html_bottom_logout_bar
		Layout.add_html_pushbuttonLogout(activeSession);
			
		MyEvents MyEventsIstance = new MyEvents(activeSession);
		
		// Viene mostrata la lista degli eventi creati dall'utente
		MyEventsIstance.show_my_events_list();
	}
	
	// Chiama la classe ShowEvent, che stamper� i dettagli dell'evento
	public void callShowEvent(LinkedList<LinkedList<String>> list){
		
		ShowEvent ShowEventIstance = new ShowEvent(activeSession);
		
		// Viene stampato a video l'evento
		ShowEventIstance.stamp_event(list);
	}
}
