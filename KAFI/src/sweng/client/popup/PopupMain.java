package sweng.client.popup;

import sweng.client.popup.comments.PopupComments;
import sweng.client.popup.login.PopupLogin;
import sweng.client.popup.note.PopupNote;
import sweng.client.popup.partecipants.PopupShowPartecipants;
import sweng.client.popup.registration.PopupRegistration;
import sweng.client.popup.search.PopupSearchEvent;
import sweng.shared.Session;

import com.google.gwt.user.client.ui.PopupPanel;


/* Classe che si occupa di istanziare differenti Popup a seconda del metodo chiamato */
public class PopupMain extends PopupPanel {
	
	// Istanzia il popup di Registrazione
	public static void createPopupRegistration(Session activeSession){
		
		PopupRegistration PopupRegistrationIstance = new PopupRegistration(activeSession);
		
		// Mostra al centro della pagina il popup di registrazione
		PopupRegistrationIstance.show_popup();
	}
	
	// Istanzia il popup di Login
	public static void createPopupLogin(Session activeSession){
		
		PopupLogin PopupLoginIstance = new PopupLogin(activeSession);
		
		// Mostra al centro della pagina il popup di login
		PopupLoginIstance.show_popup();
	}
	
	// Istanzia il popup SearchEvent
	public static void createPopupSearchEvent(Session activeSession){
		
		PopupSearchEvent PopupSearchEventIstance = new PopupSearchEvent(activeSession);
		
		// Mostra al centro della pagina il popup di login
		PopupSearchEventIstance.show_popup();
	}
	
	// Istanzia il popup ShowPartecipants
	public static void createPopupShowPartecipants(String EventID, String Date, String Hour){
		
		PopupShowPartecipants PopupShowPartecipantsIstance = new PopupShowPartecipants(EventID, Date, Hour);
		
		// Mostra al centro della pagina il popup di login
		PopupShowPartecipantsIstance.show_popup();
	}
	
	// Istanzia il popup ShowComments
	public static void createPopupComments(Session activeSession, String EventID){
		
		PopupComments PopupCommentsIstance = new PopupComments(activeSession, EventID);
		
		// Mostra al centro della pagina il popup di login
		PopupCommentsIstance.show_popup();
	}
	
	// Istanzia il popup Note
	public static void createPopupNote(Session activeSession, String EventID){
		
		PopupNote PopupNoteIstance = new PopupNote(activeSession, EventID);
		
		// Mostra al centro della pagina il popup di login
		PopupNoteIstance.show_popup();
	}
}