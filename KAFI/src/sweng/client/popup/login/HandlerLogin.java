package sweng.client.popup.login;

import java.util.LinkedList;

import sweng.client.event.MainEvent;
import sweng.client.services.LoginService;
import sweng.client.services.LoginServiceAsync;
import sweng.shared.Layout;
import sweng.shared.POJO;
import sweng.shared.Session;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;


/* Classe che gestisce l'evento Click del bottone "Log in" */
class HandlerLogin implements ClickHandler {
	  
	// Dati della sessione dell'utente
	protected Session activeSession;
	
    private PopupLogin popup;
    
    // Acquisisce l'istanza del popup per poter usufruire dei suoi widgets e la sessione attiva
    public HandlerLogin(PopupLogin popup, Session activeSession)  {
    	this.activeSession = activeSession;
    	this.popup = popup;
    }
  
    
    // All'evento Click, vengono letti Username e Password dal popup e passati al server per l'elaborazione */
	public void onClick(ClickEvent event) {
		
	    String[] arrayLogin_values = new String[2];
		
		// Disabilito il bottone del login
		popup.buttonSendLogin.setEnabled(false);
	
		// Memorizzo in un array i campi delle TextBox Username e password dal popup di Login
		arrayLogin_values[0] = popup.textboxUsername.getText();
		arrayLogin_values[1] = popup.textboxPassword.getText();
		
		// Eseguo la funzione loginToServer(String[]) per passare il testimone al Server		
		loginToServer( arrayLogin_values );
	}
	
	
	// Invia l'username e password al server tramite una chiamata RPC per l'elaborazione
	private void loginToServer(String[] arrayLogin_values) throws IllegalArgumentException {
		
		// Viene creata la Label con l'username dell'utente nell'html_user_bar
		final Label labelUsername = Layout.add_html_labelUsername();
		
		final LoginServiceAsync loginService = GWT.create(LoginService.class);
		
		loginService.loginServer(arrayLogin_values,
				new AsyncCallback<POJO>() {
					public void onFailure(Throwable caught) {
						
						// Impossibile stabilire una comunicazione RPC
						popup.labelError.setText("Fatal error!");
					}

					public void onSuccess(POJO pojoServerResponseObject) {

						// Il Login ha fallito, viene mostrato l'errore
						if(pojoServerResponseObject.getServerResponse() == 0){
							
							String error = pojoServerResponseObject.getServerResponseMessage();
							
							// Visualizzo l'errore
							popup.labelError.setText(error);
							
							// Riattivazione bottone di Login
							popup.buttonSendLogin.setEnabled(true);
						}
						
						
						// Il Login ha avuto successo
						if(pojoServerResponseObject.getServerResponse() == 1){
														
							LinkedList<LinkedList<String>> List = pojoServerResponseObject.getServerResponseList();
							
							String username = List.get(0).get(0);
							String name = List.get(0).get(1);
							
							// Inserisce l'username dell'utente creato nella labelUsername
							labelUsername.setText( "welcome, " + username );
							
							// Rimuove i bottoni "SIGN UP" e "LOG IN" dalla HTML_MENU_BAR
							RootPanel.get("html_pushbuttonRegister").clear();
							RootPanel.get("html_pushbuttonLogin").clear();
							
							// Chiude automaticamente il popup di Login
							popup.hide();
							
							// Viene modificata la sessione passando l'username dell'utente loggato
							activeSession.setSession_username(username);
							activeSession.setSession_name(name);
							
							// Passo il testimone alla classe MainEvent, passandogli la sessione appena creata
							MainEvent MainEventIstance = new MainEvent(activeSession);
							MainEventIstance.callMyEvents();
						}
					}
				});
	}
}