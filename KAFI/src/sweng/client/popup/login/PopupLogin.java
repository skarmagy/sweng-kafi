package sweng.client.popup.login;

import sweng.shared.Session;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;


// Classe che gestisce il popup di Login
public class PopupLogin extends PopupPanel {
	
		protected Session activeSession;
	
		protected Label labelTitle;
		protected Image imageGreybar;
		protected TextBox textboxUsername;
		protected PasswordTextBox textboxPassword;
		protected Label labelError;
		protected Button buttonSendLogin;
		protected HTML htmlCarriageReturn;
		protected VerticalPanel PopUpPanelContents;
		
		/* Costruttore */
	    public PopupLogin(Session activeSession) {	      
	    	super(true);
	    	this.activeSession = activeSession;
	    }
	    
	    // Visualizza il popup al centro dello schermo, impostando il layout della form di Login
	    public void show_popup(){
	    	
		      // Istanzio dei widgets che andr� ad inserire poi nel popup tramite la VerticalPanel
		      labelTitle = new Label("Log in");
		      
		      imageGreybar = new Image( GWT.getModuleBaseURL() + "gwt/clean/images/grey_bar.png");
		      
		      textboxUsername = new TextBox();
		      
		      textboxPassword = new PasswordTextBox();

		      labelError = new Label();
		      
		      buttonSendLogin = new Button();
		      
		      htmlCarriageReturn = new HTML("<br>");
		      
		      /* La vertical panel � uno strumento per inserire piu widgets
		       * Il popup puo avere solo un widgets e il vertical panel risolve il problema */
		      PopUpPanelContents = new VerticalPanel();
		      // Ordina tutti gli elementi al centro della vertical panel
		      PopUpPanelContents.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		      
		     
		      // Inserisce la label "Log in" nella parte alta del popup
		      PopUpPanelContents.add(labelTitle);
		      labelTitle.setStyleName("css-popup_toplabel");
		      
		      // Inserisce la barra verticale grigia nella parte alta del popup
		      PopUpPanelContents.add(imageGreybar);
		      
		      // Inserisci l'username
		      PopUpPanelContents.add(textboxUsername);
		      textboxUsername.getElement().setPropertyString("placeholder", "Username");
		      textboxUsername.setStyleName("css-popup_textbox");
		      
		      // Inserisci la password
		      PopUpPanelContents.add(textboxPassword);
		      textboxPassword.getElement().setPropertyString("placeholder", "Password");
		      textboxPassword.setStyleName("css-popup_textbox");
		      
		      // Label che visualizzer� eventuali errori durante la fase di Login
		      PopUpPanelContents.add(labelError);
		      labelError.setStyleName("css-generics_labelerror");
		      
		      // Inserisco un carriage return per rendere l'interfaccia del popup meno stretta
		      PopUpPanelContents.add(new HTML("<br>"));
		      
		      // Invia Login
		      buttonSendLogin.setText("Sign in"); 
		      PopUpPanelContents.add(buttonSendLogin);
		      buttonSendLogin.setStyleName("css-popup_button");
		      
		      // Carriage Return
		      PopUpPanelContents.add(htmlCarriageReturn);

		      // Aggiunge la VerticalPanel al popup, il quale visualizzer� tutto ci� che contiene
		      this.setWidget(PopUpPanelContents);
		      
		      // Richiamiamo il CSS associato
		      this.setStyleName("css-popup_popup");
		
		      // Oscura la parte sottostante al popup
		      this.setGlassEnabled(true);
			
		      // Centra il popup del Login al centro del browser
		      this.center();
			  	  
			  // Creo e gestisco l'evento Click sul bottone "Log in"
			  HandlerLogin handler = new HandlerLogin(this, activeSession);
			  buttonSendLogin.addClickHandler(handler);
	    }
 }