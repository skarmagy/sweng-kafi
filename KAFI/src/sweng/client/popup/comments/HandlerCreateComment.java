package sweng.client.popup.comments;

import sweng.client.Main;
import sweng.client.services.CreateCommentService;
import sweng.client.services.CreateCommentServiceAsync;
import sweng.shared.FieldVerifier;
import sweng.shared.POJO;
import sweng.shared.Session;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;

/* Classe che gestisce l'evento click sul bottone "Submit comment" 
 * Verifica la validit� della sessione, del commento ed infine inserisce il commento nel DB
 */
public class HandlerCreateComment implements ClickHandler{

	// Dati della sessione dell'utente
	protected Session activeSession;
	protected PopupComments popupComment;
    
    public HandlerCreateComment(PopupComments popupComment, Session activeSession)  {
    	this.activeSession = activeSession;
    	this.popupComment = popupComment;
    }
  
    
	public void onClick(ClickEvent event) {
		
		// Se la sessione non � valida l'utente viene ritornato nella main page
		if(!activeSession.isValid()){
			Main NewMainIstance = new Main();
			NewMainIstance.onModuleLoad();
			return;
		}
		
		// Viene disabilitato il bottone "Submit comment"
		popupComment.pushbuttonSubmitComment.setEnabled(false);
		
		String Comment = popupComment.textareaNewComment.getText();
		String Username = activeSession.getSession_username();
		
		
		String result_CommentValidation = FieldVerifier.validateStringFormat(Comment, 5, 100);
		 
		if(result_CommentValidation != "ok"){
			popupComment.labelError.setText("Comment too short!");
			popupComment.pushbuttonSubmitComment.setEnabled(true);
			return;
		}
		
		String[] data = new String[3];
		data[0] = popupComment.EventID;
		data[1] = Comment;
		data[2] = Username;
		
		sendCommentToServer(data);
	}
	
	private void sendCommentToServer(String[] data) {
		
		final CreateCommentServiceAsync create_commentService = GWT.create(CreateCommentService.class);
		
		create_commentService.create_commentServer(data ,
				new AsyncCallback<POJO>() {
					public void onFailure(Throwable caught) {
						
						// Impossibile stabilire una comunicazione RPC
						popupComment.labelError.setText("Fatal error!");
					}

					public void onSuccess(POJO pojoServerResponseObject) {

						if(pojoServerResponseObject.getServerResponse() == 0){
							
							popupComment.labelError.setText(pojoServerResponseObject.getServerResponseMessage());
						}
						
						
						// Il Login ha avuto successo
						if(pojoServerResponseObject.getServerResponse() == 1){
							
							popupComment.clear();
							popupComment.show_popup();
						}
					}
				});
	}
}
