package sweng.client.popup.comments;

import java.util.Iterator;
import java.util.LinkedList;

import sweng.client.services.ShowCommentsService;
import sweng.client.services.ShowCommentsServiceAsync;
import sweng.shared.POJO;
import sweng.shared.Session;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.VerticalPanel;

// Classe che gestisce il popup dei Commenti
public class PopupComments extends PopupPanel{

	protected Session activeSession;
	
	protected TextArea textareaNewComment;
	protected PushButton pushbuttonSubmitComment;
	protected Label labelError;
	
	protected String EventID;
	protected String Date;
	protected String Hour;
	
	
    public PopupComments(Session activeSession, String EventID) {
    	super(true);
    	
    	this.EventID = EventID;
    	this.activeSession = activeSession;
    }
    
    public void show_popup(){
    	
		/* La vertical panel � uno strumento per inserire piu widgets
		* Il popup puo avere solo un widgets e il vertical panel risolve il problema */
	    VerticalPanel PopUpPanelContents = new VerticalPanel();
		// Ordina tutti gli elementi al centro della vertical panel
	    PopUpPanelContents.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);

	    
    	// Titolo del popup
    	Label labelTitle = new Label("Comments");
    	
	    	// Inserisce il widget labelTitle nel "widget contenitore"
	    	PopUpPanelContents.add(labelTitle);
	    	
	    	// Vengono impostati i CSS per il widget
	    	labelTitle.setStyleName("css-popup-partecipants_topLabel_title");
	    
	    // Barra orizzontale grigia	
    	Image imageGreybar = new Image( GWT.getModuleBaseURL() + "gwt/clean/images/grey_bar.png");
	    	PopUpPanelContents.add(imageGreybar);
    	
    	
	    // Se l'utente ha effettuato l'accesso pu� inserire un commento tramite una textarea
    	if(activeSession.getSession_username() != null){
    	
	    	VerticalPanel vpNewComment = new VerticalPanel();
	           
	        	textareaNewComment = new TextArea();
	        	textareaNewComment.setCharacterWidth(12);
	        	textareaNewComment.setVisibleLines(5);
	        	
	        	
	        	textareaNewComment.getElement().setPropertyString("placeholder", "Join the discussion");
	        	textareaNewComment.setStyleName("css-popup-showcomments_textarea");
	    		
	    		vpNewComment.add(textareaNewComment);
	    		
	    		pushbuttonSubmitComment = new PushButton();
	    		pushbuttonSubmitComment.setText("Submit comment");
	    		
				// Crea e gestisce l'evento Click sul bottone "Submit comment"
				HandlerCreateComment handler = new HandlerCreateComment(this, activeSession);
	    		pushbuttonSubmitComment.addClickHandler(handler);
	    		pushbuttonSubmitComment.getElement().getStyle().setProperty("textAlign", "center");
	    		
	    		vpNewComment.add(pushbuttonSubmitComment);
	    		
	    	    PopUpPanelContents.add(vpNewComment);
    	}
    		
    	VerticalPanel vpCommentsList = new VerticalPanel();
    	
    	labelError = new Label();
    		labelError.setStyleName("css-generics_padding");
    	
    	// Viene mostrata la lista dei commenti gi� inseriti nel popup
    	showComments(vpCommentsList, EventID); 	
    	
    	// CarriageReturn per evitare che la zona bottom del popup sia troppo stretta
	    HTML CarriageReturn = new HTML("<br>");
	      

	    // Vengono aggiunti al "widget container" i widgets da visualizzare nel popup
	    PopUpPanelContents.add(vpCommentsList);	    
	    PopUpPanelContents.add(labelError);
	    PopUpPanelContents.add(CarriageReturn);
	    
	    
	    // Il "widget container" viene inserito in una ScrollPanel, che permette
	    // di muoversi su e giu nella visualizzazione di tutti i widgets
    	ScrollPanel panel = new ScrollPanel(PopUpPanelContents);
    		panel.setSize("280px", "400px");
	    
	    // Aggiunge la ScrollPanel al popup, il quale visualizzer� tutto ci� che contiene
	    this.setWidget(panel);
	      
	    // Richiamiamo il CSS associato
	    this.setStyleName("css-popup_popup");
	
	    // Oscura la parte sottostante al popup
	    this.setGlassEnabled(true);

	    // Centra il popup nel browser
	    this.center();
    }
    
    
    // Inserisce nella VerticalPanel vpCommentsList gli eventuali commenti gi� inseriti dagli utenti per l'evento
    private void showComments(final VerticalPanel vpCommentsList, String EventID){
    	
		final ShowCommentsServiceAsync show_commentsService = GWT.create(ShowCommentsService.class);	 
		
		show_commentsService.show_commentsServer(EventID,
				new AsyncCallback<POJO>() {
					public void onFailure(Throwable caught) {
						
						// Impossibile stabilire una comunicazione RPC
						labelError.setText("Fatal error!");
					}
					
					public void onSuccess(POJO pojoServerResponseObject) {
						
						// Il server risponde non trovando nessun commento relativo all'evento
						if(pojoServerResponseObject.getServerResponse() == 0){
	
							labelError.setText(pojoServerResponseObject.getServerResponseMessage());
						}
						
						// Il server ha trovato uno o pi� messaggi relativi all'evento
						// Ritorna una lista di liste con i commenti ed i loro creatori
						if(pojoServerResponseObject.getServerResponse() == 1){
						
							// Lista di liste ritornata dal server
							LinkedList<LinkedList<String>> list = pojoServerResponseObject.getServerResponseList();
							
							// Creo un iteratore alla lista principale che servir� per scorrere gli elementi della lista
							Iterator<LinkedList<String>> List_iter = list.listIterator();
							
							// Punto l'iteratore della lista principale alla prima lista
							LinkedList<String> SubList = List_iter.next();
							
							// Creo un iteratore alla lista figlia puntata dall'iteratore principale
							Iterator<String> SubList_iter = SubList.listIterator();
							
							
							boolean first_iteraction = true;

							
							do{
								
								// Finch� esiste un elemento nella lista, questo viene inserito nella vpPanel
								VerticalPanel vpPanel = new VerticalPanel();
								
								if(!first_iteraction){
									SubList = List_iter.next();
									SubList_iter = SubList.listIterator();
								}
								
								String username = SubList_iter.next();
								String comment = SubList_iter.next();

								Label labelUser = new Label(username + ", wrote:");											
								Label labelComment = new Label(comment);
								
								labelUser.setStyleName("css-popup-showcomments_padding");
								labelComment.setStyleName("css-popup-showcomments_comment");
								
								vpPanel.add(labelUser);
								vpPanel.add(labelComment);
								
								// Vengono inseriti nel "widget container" vpCommentsList
								// i due widgets che contengono rispettivamente l'username e il commento associato
								vpCommentsList.add(vpPanel);
								
								first_iteraction = false;
							
							}while(List_iter.hasNext());
						}
					}
				});
    }

}