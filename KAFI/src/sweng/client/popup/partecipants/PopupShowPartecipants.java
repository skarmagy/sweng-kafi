package sweng.client.popup.partecipants;

import java.util.Iterator;
import java.util.LinkedList;

import sweng.client.services.PartecipantDetailService;
import sweng.client.services.PartecipantDetailServiceAsync;
import sweng.shared.POJO;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

/* Mostra il popup con le persone che partecipano ad un determinato evento */
public class PopupShowPartecipants extends PopupPanel{

	protected String EventID;
	protected String Date;
	protected String Hour;
	
	protected Label labelError;
	
    public PopupShowPartecipants(String EventID, String Date, String Hour) {
    	super(true);
    	
    	this.EventID = EventID;
    	this.Date = Date;
    	this.Hour = Hour;
    }
    
    public void show_popup(){
    	
    	// Istanzio dei widgets che andr� ad inserire poi nel popup tramite la VerticalPanel
    	Label labelTitle = new Label("Partecipants");
	    
    	Image imageGreybar = new Image( GWT.getModuleBaseURL() + "gwt/clean/images/grey_bar.png");
    	
    	VerticalPanel vpPartecipants = new VerticalPanel();
    	
    	stampPartecipantsList(vpPartecipants, EventID, Date, Hour);
    	
    	labelError = new Label();
    	
	    HTML CarriageReturn = new HTML("<br>");
	    
		/* La vertical panel � uno strumento per inserire piu widgets
		* Il popup puo avere solo un widgets e il vertical panel risolve il problema */
	    VerticalPanel PopUpPanelContents = new VerticalPanel();
		// Ordina tutti gli elementi al centro della vertical panel
	    PopUpPanelContents.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
	    
	    // Inserisce la label "Partecipants" nella parte alta del popup
	    PopUpPanelContents.add(labelTitle);
	    labelTitle.setStyleName("css-popup-partecipants_topLabel_title");
	      
	    // Inserisce la barra verticale grigia nella parte alta del popup
	    PopUpPanelContents.add(imageGreybar);
	    
	    PopUpPanelContents.add(vpPartecipants);
	    
	    PopUpPanelContents.add(labelError);
	    
	    PopUpPanelContents.add(CarriageReturn);
	    
    	ScrollPanel scrollpanel_container = new ScrollPanel(PopUpPanelContents);
    	scrollpanel_container.setSize("280px", "400px");
	    
	    // Aggiunge la VerticalPanel al popup, il quale visualizzer� tutto ci� che contiene
	    this.setWidget(PopUpPanelContents);
	      
	    // Richiamiamo il CSS associato
	    this.setStyleName("css-popup_popup");
	
	    // Oscura la parte sottostante al popup
	    this.setGlassEnabled(true);

	    // Centra il popup della registrazione al centro del browser
	    this.center();
    }
    
    private void stampPartecipantsList(final VerticalPanel vpPartecipants, String EventID, String Date, String Hour){
    	
		String[] data = new String[3];
		data[0] = EventID;
		data[1] = Date;
		data[2] = Hour;

    	
		final PartecipantDetailServiceAsync partecipant_detailService = GWT.create(PartecipantDetailService.class);	 
		
		partecipant_detailService.partecipant_detailServer(data,
				new AsyncCallback<POJO>() {
					public void onFailure(Throwable caught) {
						
						// Impossibile stabilire una comunicazione RPC
						labelError.setText("Fatal error!");
					}
					
					public void onSuccess(POJO pojoServerResponseObject) {
						
						if(pojoServerResponseObject.getServerResponse() == 0){
	
							labelError.setText(pojoServerResponseObject.getServerResponseMessage());
						}
						
						
						if(pojoServerResponseObject.getServerResponse() == 1){
						
							LinkedList<LinkedList<String>> list = pojoServerResponseObject.getServerResponseList();
							
							// Creo un iteratore alla lista principale che servir� per scorrere gli elementi della lista
							Iterator<LinkedList<String>> List_iter = list.listIterator();
							
							// Punto l'iteratore della lista principale alla prima lista
							LinkedList<String> SubList = List_iter.next();
							
							// Creo un iteratore alla lista figlia puntata dall'iteratore principale
							Iterator<String> SubList_iter = SubList.listIterator();
							
							HorizontalPanel hpPanel = new HorizontalPanel();
							
							Label labelTitleUsername = new Label("USERNAME");
							Label labelTitleName = new Label("NAME");
							
							labelTitleUsername.setStyleName("css-popup-partecipants_title");
							labelTitleName.setStyleName("css-popup-partecipants_title");
							
							hpPanel.add(labelTitleUsername);
							hpPanel.add(labelTitleName);
							
							vpPartecipants.add(hpPanel);
							
							boolean first_iteraction = true;
							
							do{
								
								hpPanel = new HorizontalPanel();
								
								if(!first_iteraction){
									SubList = List_iter.next();
									SubList_iter = SubList.listIterator();
								}
								
								String name = SubList_iter.next();
								String username = null;
								
								Label labelUsername = new Label();
								
								if(SubList_iter.hasNext()){
									
									username = SubList_iter.next();
									labelUsername.setText(username);;
								}
								
								Label labelName = new Label(name);
								
								labelUsername.setStyleName("css-popup-partecipants_content");
								labelName.setStyleName("css-popup-partecipants_content");
								
								hpPanel.add(labelUsername);
								hpPanel.add(labelName);
								
								vpPartecipants.add(hpPanel);
								vpPartecipants.setStyleName("css-popup-partecipants_panel_border");
								
								first_iteraction = false;
																
							}while(List_iter.hasNext());
						}
					}
				});
    }
}
