package sweng.client.popup.registration;

import java.util.LinkedList;

import sweng.client.event.MainEvent;
import sweng.client.popup.registration.PopupRegistration;
import sweng.client.services.RegisterService;
import sweng.client.services.RegisterServiceAsync;
import sweng.shared.Layout;
import sweng.shared.POJO;
import sweng.shared.Session;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;


/* Classe che gestisce l'evento Click del bottone "Sign up" */
class HandlerRegistration implements ClickHandler {
	  
	// Dati della sessione dell'utente
	protected Session activeSession;
	
    private PopupRegistration popup;
    
    // Acquisisce l'istanza del popup per poter usufruire dei suoi widgets e la sessione attiva
    public HandlerRegistration(PopupRegistration popup, Session activeSession)  {
        this.activeSession = activeSession;
    	this.popup = popup;
    }
  
    
    // All'evento Click, vengono letti Username,Password,... dal popup e passati al server per l'elaborazione
	public void onClick(ClickEvent event) {
		
	    String[] arrayRegister_values = new String[5];
		
	    // Disabilito il bottone della registrazione
		popup.buttonSendRegistration.setEnabled(false);
	
		// Memorizzo in un array i campi delle TextBox Username,password,.. dal popup di Registrazione
		arrayRegister_values[0] = popup.textboxUsername.getText();
		arrayRegister_values[1] = popup.textboxPassword.getText();
		arrayRegister_values[2] = popup.textboxConfirmPassword.getText();
		arrayRegister_values[3] = popup.textboxName.getText();
		arrayRegister_values[4] = popup.textboxEmail.getText();
		
		// Eseguo la funzione registerToServer(String[]) per passare il testimone al Server			
		registerToServer( arrayRegister_values );
	}
	
	
	// Invia l'username,password,.. al server tramite una chiamata RPC per l'elaborazione
	private void registerToServer(String[] arrayRegister_values) {
		
		// Viene creata la Label con l'username dell'utente nell'html_user_bar
		final Label labelUsername = Layout.add_html_labelUsername();
		
		final RegisterServiceAsync registerService = GWT.create(RegisterService.class);	 
		
		registerService.registerServer(arrayRegister_values,
				new AsyncCallback<POJO>() {
					public void onFailure(Throwable caught) {
						
						// Impossibile stabilire una comunicazione RPC
						popup.labelError.setText("Fatal error!");
					}
					
					
					public void onSuccess(POJO pojoServerResponseObject) {
						
						// La registrazione non ha avuto successo
						if(pojoServerResponseObject.getServerResponse() == 0){
							
							// Visualizzo l'errore
							popup.labelError.setText(pojoServerResponseObject.getServerResponseMessage());
							
							// Riattivazione bottone di Login
							popup.buttonSendRegistration.setEnabled(true);
						}
						
						
						// La registrazione ha avuto successo
						if(pojoServerResponseObject.getServerResponse() == 1){
							
							LinkedList<LinkedList<String>> List = pojoServerResponseObject.getServerResponseList();
							
							String username = List.get(0).get(0);
							String name = List.get(0).get(1);
							
							// Inserisce l'username dell'utente creato nella labelUsername
							labelUsername.setText( "welcome, " + username );

							// Rimuove i bottoni "SIGN UP" e "LOG IN" dalla HTML_MENU_BAR
							RootPanel.get("html_pushbuttonRegister").clear();
							RootPanel.get("html_pushbuttonLogin").clear();
							
							// Chiude automaticamente il Popup di Registrazione aperto
							popup.hide();
							
							// Viene modificata la sessione passando l'username dell'utente loggato
							activeSession.setSession_username(username);
							activeSession.setSession_name(name);
							
							// Passo il testimone alla classe MainEvent, passandogli la sessione appena creata
							MainEvent MainEventIstance = new MainEvent(activeSession);
							MainEventIstance.callMyEvents();
						}
					}
				});
	}
}
