package sweng.client.popup.registration;

import sweng.shared.Session;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;


/* Classe che gestir� il popup di registrazione */
public class PopupRegistration extends PopupPanel {

		protected Session activeSession;
	
	    protected Label labelRegister_top;
	    protected Image imageGreybar;
	    protected TextBox textboxUsername;
	    protected PasswordTextBox textboxPassword;
	    protected PasswordTextBox textboxConfirmPassword ;
	    protected TextBox textboxName;
	    protected TextBox textboxEmail;
	    protected Label labelError;
	    protected Button buttonSendRegistration;
	    protected HTML CarriageReturn;
	    protected VerticalPanel PopUpPanelContents;
	
	    
	    public PopupRegistration(Session activeSession) {
	    	super(true);
	    	this.activeSession = activeSession;
	    }
	    
	    // Visualizza il popup al centro dello schermo, impostando il layout della form di Registrazione
	    public void show_popup(){
	    	
		      // Istanzio dei widgets che andr� ad inserire poi nel popup tramite la VerticalPanel
		      labelRegister_top = new Label("Sign in");
		      
		      imageGreybar = new Image( GWT.getModuleBaseURL() + "gwt/clean/images/grey_bar.png");
		      
		      textboxUsername = new TextBox();
		      
		      textboxPassword = new PasswordTextBox();
		      
		      textboxConfirmPassword = new PasswordTextBox();
		      
		      textboxName = new TextBox();
		      
		      textboxEmail = new TextBox();

		      labelError = new Label();
		      
		      buttonSendRegistration = new Button();
		      
		      CarriageReturn = new HTML("<br>");

		      /* La vertical panel � uno strumento per inserire piu widgets
		       * Il popup puo avere solo un widgets e il vertical panel risolve il problema */
		      PopUpPanelContents = new VerticalPanel();
		      // Ordina tutti gli elementi al centro della vertical panel
		      PopUpPanelContents.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		      
		      
		      // Inserisce la label "Sign up" nella parte alta del popup
		      PopUpPanelContents.add(labelRegister_top);
		      labelRegister_top.setStyleName("css-popup_toplabel");
		      
		      // Inserisce la barra verticale grigia nella parte alta del popup
		      PopUpPanelContents.add(imageGreybar);
		      
		      // Inserisci l'username
		      PopUpPanelContents.add(textboxUsername);
		      textboxUsername.getElement().setPropertyString("placeholder", "Username");
		      textboxUsername.setStyleName("css-popup_textbox");
		      
		      // Inserisci la password
		      PopUpPanelContents.add(textboxPassword);
		      textboxPassword.getElement().setPropertyString("placeholder", "Password");
		      textboxPassword.setStyleName("css-popup_textbox");
		      
		      // Inserisci la conferma della password
		      PopUpPanelContents.add(textboxConfirmPassword);
		      textboxConfirmPassword.getElement().setPropertyString("placeholder", "Confirm your password");
		      textboxConfirmPassword.setStyleName("css-popup_textbox");
		      
		      // Inserisci il nome / nickname
		      PopUpPanelContents.add(textboxName);
		      textboxName.getElement().setPropertyString("placeholder", "Name or nickname");
		      textboxName.setStyleName("css-popup_textbox");
		      
		      // Inserisci l'email
		      PopUpPanelContents.add(textboxEmail);
		      textboxEmail.getElement().setPropertyString("placeholder", "Email Address (Optional)");
		      textboxEmail.setStyleName("css-popup_textbox");
		      
		      // Label che visualizzer� eventuali errori durante la fase di registrazione
		      PopUpPanelContents.add(labelError);
		      labelError.setStyleName("css-generics_labelerror");
		      
		      // Inserisco un carriage return per rendere l'interfaccia del popup meno stretta
		      PopUpPanelContents.add(new HTML("<br>"));
		      
		      // Invia registrazione
		      buttonSendRegistration.setText("Sign up"); 
		      PopUpPanelContents.add(buttonSendRegistration);
		      buttonSendRegistration.setStyleName("css-popup_button");
		      
		      // Carriage Return
		      PopUpPanelContents.add(CarriageReturn);

		      // Aggiunge la VerticalPanel al popup, il quale visualizzer� tutto ci� che contiene
		      this.setWidget(PopUpPanelContents);
		      
		      // Richiamiamo il CSS associato
		      this.setStyleName("css-popup_popup");
		
		      // Oscura la parte sottostante al popup
		      this.setGlassEnabled(true);

		      // Centra il popup della registrazione al centro del browser
		      this.center();
			  
			  // Creo e gestisco l'evento Click sul bottone "Sign up"
			  HandlerRegistration handler = new HandlerRegistration(this, activeSession);
			  buttonSendRegistration.addClickHandler(handler);
	    }
}