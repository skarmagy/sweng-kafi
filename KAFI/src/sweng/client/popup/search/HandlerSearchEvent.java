package sweng.client.popup.search;

import java.util.LinkedList;

import sweng.client.event.MainEvent;
import sweng.client.services.SearchEventService;
import sweng.client.services.SearchEventServiceAsync;
import sweng.shared.POJO;
import sweng.shared.Session;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;

/* Classe che gestisce l'evento Click del bottone "Search event" */
public class HandlerSearchEvent implements ClickHandler{

	// Dati della sessione dell'utente
	protected Session activeSession;
	
	protected PopupSearchEvent popup;
	
	// Acquisisce l'istanza del popup per poter usufruire dei suoi widgets e la sessione attiva
	public HandlerSearchEvent(PopupSearchEvent popup, Session activeSession){
		this.activeSession = activeSession;
		this.popup = popup;
	}
	
    // All'evento Click viene letto l'ID dell'evento e passato al server per l'elaborazione
	public void onClick(ClickEvent event) {
		
		String EventID = popup.textboxEventID.getText();
		
	    // Disabilito il bottone di ricerca evento 
		popup.buttonSendRequest.setEnabled(false);

		// Eseguo la funzione registerToServer(String[]) per passare il testimone al Server			
		SearchEventToServer(EventID);
}
	
	// Invia l'ID dell'evento al server tramite una chiamata RPC per l'elaborazione
	private void SearchEventToServer(String EventID) {
		
		final SearchEventServiceAsync searcheventService = GWT.create(SearchEventService.class);	 
		
		searcheventService.searcheventServer(EventID,
				new AsyncCallback<POJO>() {
					public void onFailure(Throwable caught) {
						
						// Impossibile stabilire una comunicazione RPC
						popup.labelError.setText("Fatal error!");
					}
					
					public void onSuccess(POJO pojoServerResponseObject) {
						
						// L'evento non � stato trovato oppure � stato generato un errore con il database
						if(pojoServerResponseObject.getServerResponse() == 0){
							
							// Visualizzo l'errore
							popup.labelError.setText(pojoServerResponseObject.getServerResponseMessage());
						
							// Riattivazione bottone "Search event"
							popup.buttonSendRequest.setEnabled(true);
						}
						
						
						// L'evento � stato trovato
						if(pojoServerResponseObject.getServerResponse() == 1){
							
							// Chiude automaticamente il Popup SearchEvent aperto
							popup.hide();
														
							LinkedList<LinkedList<String>> listEventDetails = pojoServerResponseObject.getServerResponseList();
							
							MainEvent MainEventIstance = new MainEvent(activeSession);
							MainEventIstance.callShowEvent(listEventDetails);
						}
					}
				});
	}
}
