package sweng.client.popup.search;

import sweng.shared.Session;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

/* Classe che gestir� il popup di ricerca eventi */
public class PopupSearchEvent extends PopupPanel{

	protected Session activeSession;
	
    protected Label labelSearchEvent_top;
    protected Image imageGreybar;
    protected Label labelText;
    protected TextBox textboxEventID;
    protected Label labelError;
    protected Button buttonSendRequest;
    protected HTML CarriageReturn;
    protected VerticalPanel PopUpPanelContents;
	
    public PopupSearchEvent(Session activeSession) {
    	super(true);
    	this.activeSession = activeSession;
    }
    
    // Visualizza il popup al centro dello schermo, impostando il layout della form SearchEvent
    public void show_popup(){
    	
    	// Istanzio dei widgets che andr� ad inserire poi nel popup tramite la VerticalPanel
    	labelSearchEvent_top = new Label("Search Event");
	      
    	imageGreybar = new Image( GWT.getModuleBaseURL() + "gwt/clean/images/grey_bar.png");
  
    	labelText = new Label();
    	
    	textboxEventID = new TextBox();

    	labelError = new Label();
  
    	buttonSendRequest = new Button();
  
    	CarriageReturn = new HTML("<br>");
    	
	    /* La vertical panel � uno strumento per inserire piu widgets
	     * Il popup puo avere solo un widgets e il vertical panel risolve il problema */
	    PopUpPanelContents = new VerticalPanel();
	    // Ordina tutti gli elementi al centro della vertical panel
	    PopUpPanelContents.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
	    
	    // Inserisce la label "Sign up" nella parte alta del popup
	    PopUpPanelContents.add(labelSearchEvent_top);
	    labelSearchEvent_top.setStyleName("css-popup_toplabel");
	      
	    // Inserisce la barra verticale grigia nella parte alta del popup
	    PopUpPanelContents.add(imageGreybar);
	    
	    // EventID textbox
	    PopUpPanelContents.add(textboxEventID);
	    textboxEventID.getElement().setPropertyString("placeholder", "Event ID");
	    textboxEventID.setStyleName("css-popup_textbox");
	     
	    // Label che visualizzer� eventuali errori durante la fase di registrazione
	    PopUpPanelContents.add(labelError);
	    labelError.setStyleName("css-generics_labelerror");
	      
	    // Inserisco un carriage return per rendere l'interfaccia del popup meno stretta
	    PopUpPanelContents.add(new HTML("<br>"));
	      
	    // Invia registrazione
	    buttonSendRequest.setText("Search event"); 
	    PopUpPanelContents.add(buttonSendRequest);
	    buttonSendRequest.setStyleName("css-popup_button");
	      
	    // Carriage Return
	    PopUpPanelContents.add(CarriageReturn);

	    // Aggiunge la VerticalPanel al popup, il quale visualizzer� tutto ci� che contiene
	    this.setWidget(PopUpPanelContents);
	      
	    // Richiamiamo il CSS associato
	    this.setStyleName("css-popup_popup");
	
	    // Oscura la parte sottostante al popup
	    this.setGlassEnabled(true);

	    // Centra il popup della registrazione al centro del browser
	    this.center();
		  
		// Creo e gestisco l'evento Click sul bottone "Sign up"
		HandlerSearchEvent handler = new HandlerSearchEvent(this, activeSession);
		buttonSendRequest.addClickHandler(handler);
    	
    }
}
