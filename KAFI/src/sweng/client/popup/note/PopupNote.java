package sweng.client.popup.note;

import sweng.client.event.myevents.stamp.close.HandlerCloseEvent;
import sweng.shared.Session;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.VerticalPanel;


/* Classe che gestir� il popup Note */
public class PopupNote extends PopupPanel {

		protected Session activeSession;
		
		public String EventID;
	
		public TextArea textareaNote;
		public PushButton pushbuttonCreateNote;
		public PushButton pushbuttonSkip;
		public Label labelError;
	    
	    public PopupNote(Session activeSession, String EventID) {
	    	super(true);
	    	this.activeSession = activeSession;
	    	this.EventID = EventID;
	    }
	    
	    // Visualizza il popup al centro dello schermo, impostando il layout della form di Registrazione
	    public void show_popup(){

		    // Istanzio dei widgets che andr� ad inserire poi nel popup tramite la VerticalPanel
		    Label labelNote = new Label("Note");
		    	labelNote.setStyleName("css-popup-partecipants_topLabel_title");
		    
		    Image imageGreybar = new Image( GWT.getModuleBaseURL() + "gwt/clean/images/grey_bar.png");
		    
		    textareaNote = new TextArea();
			    textareaNote.setCharacterWidth(12);
	        	textareaNote.setVisibleLines(2);
	        	textareaNote.getElement().setPropertyString("placeholder", "Add a note here");
	        	textareaNote.setStyleName("css-popup-note_textarea");
		    
		    HorizontalPanel hpPanel = new HorizontalPanel();
		    
		    	pushbuttonCreateNote = new PushButton();
		    	pushbuttonCreateNote.setText("Add a note");
		    	pushbuttonCreateNote.setStyleName("css-popup-note_button");
		    	
		    	pushbuttonSkip = new PushButton();
		    	pushbuttonSkip.setText("Skip");
		    	pushbuttonSkip.setStyleName("css-popup-note_button");
		    	
		    	
		    	hpPanel.add(pushbuttonCreateNote);
		    	hpPanel.add(pushbuttonSkip);
		    	
		    	
		    
		    labelError = new Label();
		    	labelError.setStyleName("css-generics_labelerror");
		    
		    HTML CarriageReturn = new HTML("<br>");

		    /* La vertical panel � uno strumento per inserire piu widgets
		    * Il popup puo avere solo un widgets e il vertical panel risolve il problema */
		    VerticalPanel PopUpPanelContents = new VerticalPanel();
		    // Ordina tutti gli elementi al centro della vertical panel
		    PopUpPanelContents.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		      
		      
		    // Inserisce la label "Sign up" nella parte alta del popup
		    PopUpPanelContents.add(labelNote);
		      
		    // Inserisce la barra verticale grigia nella parte alta del popup
		    PopUpPanelContents.add(imageGreybar);
		    
		    PopUpPanelContents.add(textareaNote);
		    PopUpPanelContents.add(labelError);
		    PopUpPanelContents.add(hpPanel);	
		    
		    // Carriage Return
		    PopUpPanelContents.add(CarriageReturn);

		    
		    // Il "widget container" viene inserito in una ScrollPanel, che permette
		    // di muoversi su e giu nella visualizzazione di tutti i widgets
	    	ScrollPanel panel = new ScrollPanel(PopUpPanelContents);
	    		panel.setSize("280px", "270px");
		    
		    // Aggiunge la ScrollPanel al popup, il quale visualizzer� tutto ci� che contiene
		    this.setWidget(panel);
		      
		    // Richiamiamo il CSS associato
		    this.setStyleName("css-popup_popup");
		
		    // Oscura la parte sottostante al popup
		    this.setGlassEnabled(true);

		    // Centra il popup della registrazione al centro del browser
		    this.center();
			  
		    HandlerCloseEvent skip_handler = new HandlerCloseEvent(activeSession, EventID, this, false);
		    HandlerCloseEvent addNote_handler = new HandlerCloseEvent(activeSession, EventID, this, true);
		    
		    pushbuttonCreateNote.addClickHandler(addNote_handler);
		    pushbuttonSkip.addClickHandler(skip_handler);
	    }
}
