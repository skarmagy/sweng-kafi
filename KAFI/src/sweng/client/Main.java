package sweng.client;

import sweng.shared.Layout;
import sweng.shared.Session;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;


/* Classe di partenza */
public class Main implements EntryPoint{

	public void onModuleLoad() {
		
		// Viene creata la sessione
		Session activeSession = new Session();
		
		// Vengono eliminati dalla pagina tutti gli eventuali widgets presenti
		Layout.clean_everything();
		
		// Vengono aggiunti i bottoni "SIGN UP", "LOG IN" e "SEARCH EVENT" nella html_menu_bar
		Layout.add_html_pushbuttonSearchEvent(activeSession);;
		Layout.add_html_pushbuttonRegister(activeSession);
		Layout.add_html_pushbuttonLogin(activeSession);
		
		
		// Layout della Main page
		VerticalPanel vpPanel = new VerticalPanel();
		
			Label labelText_1 = new Label();
			labelText_1.setText("Create your event and share it for free");
			
			Label labelText_2 = new Label();
			labelText_2.setText("a project made by Karmagy Sami");
					
			Image imageGreybar = new Image( GWT.getModuleBaseURL() + "gwt/clean/images/grey_bar.png");
			
			Label labelText_3 = new Label();
			labelText_3.setText("JME");
			
			Image imageSubLogo = new Image( GWT.getModuleBaseURL() + "gwt/clean/images/jme_sub-logo.png");
			
			
			labelText_1.setStyleName("css-main_labelText_1");
			labelText_2.setStyleName("css-main_labelText_2");
			imageGreybar.setStyleName("css-main_Greybar");
			labelText_3.setStyleName("css-main_labelText_3");
			imageSubLogo.setStyleName("css-main_SubLogo");
			
			
		vpPanel.add(labelText_1);
		vpPanel.add(labelText_2);
		vpPanel.add(imageGreybar);
		vpPanel.add(labelText_3);
		vpPanel.add(imageSubLogo);
			
			
		vpPanel.setStyleName("css-main_vppanel_padding");
		
		RootPanel.get("html_main_content_middle").add(vpPanel);
	}
}
