package sweng.server;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;

import sweng.client.services.ShowCommentsService;
import sweng.shared.Database;
import sweng.shared.POJO;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;


/* Dato l'ID di un evento, ritorna la lista dei commenti scritti dagli utenti  */

@SuppressWarnings("serial")
public class ShowCommentsServiceImpl extends RemoteServiceServlet implements ShowCommentsService {
	
	public POJO show_commentsServer(String EventID) throws IllegalArgumentException {
		
		// Oggetto che conterr� i dati di risposta del server
		POJO pojoServerResult = new POJO();
		
		// Lista di liste che conterr� tutti gli eventi e i loro parametri associati
		LinkedList<LinkedList<String>> list = new LinkedList<LinkedList<String>>();
		
		// ArrayList che conterr� i parametri da inserire nella query
		ArrayList<String> arraylistQueryRecords = new ArrayList<String>();
		
		ResultSet resultsetQuery_result = null;
		
		arraylistQueryRecords.clear();
		arraylistQueryRecords.add(EventID);

		// Esegue la SELECT [Tabella: Commenti]
		resultsetQuery_result = Database.select_query(
					"SELECT * FROM Commenti WHERE id = ?",
					1,
					arraylistQueryRecords);
		
		// Se non viene trovato nemmeno un commento
		if( resultsetQuery_result == null){
			pojoServerResult.setServerResponse(0);
			pojoServerResult.setServerResponseMessage("No comments posted yet.");
			return pojoServerResult;
		}
		
		
		try {
						
			int cont = 0;
			
			do{
				
				list.add(new LinkedList<String>());
				
				String Username = resultsetQuery_result.getString("username");
				String Commento = resultsetQuery_result.getString("commento");
				
				list.get(cont).add(Username);
				list.get(cont).add(Commento);

				cont++;
				
			}while(resultsetQuery_result.next());
		
		} catch (SQLException e) {
			pojoServerResult.setServerResponse(0);
			pojoServerResult.setServerResponseMessage("Fatal error! #SQL Exception");
			return pojoServerResult;
		}
		
		// Ritorna la lista dei commenti scritti dagli utenti
		pojoServerResult.setServerResponse(1);
		pojoServerResult.setServerResponseMessage("ok");
		pojoServerResult.setServerResponseList(list);
		return pojoServerResult;
	}
}
