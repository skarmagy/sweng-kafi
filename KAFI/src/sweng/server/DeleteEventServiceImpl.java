package sweng.server;

import java.util.ArrayList;

import sweng.client.services.DeleteEventService;
import sweng.shared.Database;
import sweng.shared.POJO;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;


/* Classe server-side che elabora i dati ricevuti dall'istanza della classe HandlerDeleteEvent
 * Elimina dalle tabelle Orari ed Eventi i record contenenti l'ID dell'evento passato */

@SuppressWarnings("serial")
public class DeleteEventServiceImpl extends RemoteServiceServlet implements DeleteEventService {
	
	public POJO deleteeventServer(String EventID) throws IllegalArgumentException {
		
		// Oggetto che conterr� i dati di risposta del server
		POJO pojoServerResult = new POJO();
		
		// Lista che contiene i parametri della query
		ArrayList<String> List_db_query = new ArrayList<String>();
		
		List_db_query.clear();
		List_db_query.add(EventID);
		
		// Esegue la DELETE [Tabella: Eventi]
		// "ok" se ha avuto successo
		// restituisce l'errore altrimenti
		String stringQuery_result = Database.delete_query(
					"DELETE FROM Eventi WHERE id=?",
					1,
					List_db_query);
		
		// Controllo se la query ha avuto successo, altrimenti restituisce l'errore associato
		if( stringQuery_result != "ok"){
			pojoServerResult.setServerResponse(0);
			pojoServerResult.setServerResponseMessage(stringQuery_result);
			return pojoServerResult;
		}
		
		
		List_db_query.clear();
		List_db_query.add(EventID);
		
		// Esegue la DELETE [Tabella: Orari]
		// "ok" se ha avuto successo
		// restituisce l'errore altrimenti
		stringQuery_result = Database.delete_query(
					"DELETE FROM Orari WHERE id=?",
					1,
					List_db_query);
		
		// Controllo se la query ha avuto successo, altrimenti restituisce l'errore associato
		if( stringQuery_result != "ok"){
			pojoServerResult.setServerResponse(0);
			pojoServerResult.setServerResponseMessage(stringQuery_result);
			return pojoServerResult;
		}
		
		// Le query hanno avuto successo
		pojoServerResult.setServerResponse(1);
		pojoServerResult.setServerResponseMessage("ok");
		return pojoServerResult;
	}
}
