package sweng.server;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;

import sweng.client.services.SearchEventService;
import sweng.shared.Database;
import sweng.shared.POJO;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/* Classe che dato un ID cerca nel database se esiste l'evento associato */ 

@SuppressWarnings("serial")
public class SearchEventServiceImpl extends RemoteServiceServlet implements SearchEventService {

	public POJO searcheventServer(String EventID) throws IllegalArgumentException{
		
		// Oggetto che conterr� i dati di risposta del server
		POJO pojoServerResult = new POJO();
		
		// Viene controllato il formato dell'ID, cio� che sia esattamente di 7 caratteri
		if(EventID.length() != 7){
			pojoServerResult.setServerResponse(0);
			pojoServerResult.setServerResponseMessage("Wrong ID format");
			return pojoServerResult;
		}
		
		// ArrayList che conterr� i parametri da inserire nella query
		ArrayList<String> arraylistQueryRecords = new ArrayList<String>();
		
		// Variabile che conterr� il risultato delle query (i record)
		ResultSet resultsetQuery = null;
		
		
		///////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////		
		arraylistQueryRecords.clear();
		arraylistQueryRecords.add(EventID);
		
		// Esegue la SELECT [Tabella: Eventi]
		// ResultSet se � stato trovato il record nella tabella
		// "null" se non esiste il record nella tabella
		resultsetQuery = Database.select_query(
					"SELECT * FROM Eventi WHERE id = ?",
					1,
					arraylistQueryRecords);
		
		// Se il risultato della query � null significa che non esiste nessun evento con quell'ID
		if(resultsetQuery == null){
			pojoServerResult.setServerResponse(0);
			pojoServerResult.setServerResponseMessage("Event not founded");
			return pojoServerResult;
		}
		
		///////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////
		
		// Lista di liste che conterr� tutti gli eventi e i loro parametri associati
		LinkedList<LinkedList<String>> list = new LinkedList<LinkedList<String>>();

		
		try {	
			
			String nome_evento = resultsetQuery.getString("nome_evento");
			String luogo_evento = resultsetQuery.getString("luogo_evento");
			String descrizione_evento = resultsetQuery.getString("descrizione_evento");
			String username = resultsetQuery.getString("username");
			String aperto = resultsetQuery.getString("aperto");
			String nota = resultsetQuery.getString("nota");
			
			// Inserisce nella prima lista i dati relativi all'evento, come l'ID, il nome, il luogo, ecc..
			list.add(new LinkedList<String>());
			list.get(0).add(EventID);
			list.get(0).add(nome_evento);
			list.get(0).add(luogo_evento);
			list.get(0).add(descrizione_evento);
			list.get(0).add(username);
			list.get(0).add(aperto);
			list.get(0).add(nota);
		
			
			arraylistQueryRecords.clear();
			arraylistQueryRecords.add(EventID);
	
			// Esegue la SELECT [Tabella: Orari]
			// ResultSet se � stato trovato il record nella tabella
			// "null" se non esiste il record nella tabella
			resultsetQuery = Database.select_query(
						"SELECT * FROM Orari WHERE id = ?",
						1,
						arraylistQueryRecords);
			
			// Se il risultato � null allora ad un evento creato non sono associate date, ed � un errore gravissimo 
			if(resultsetQuery == null){
				pojoServerResult.setServerResponse(0);
				pojoServerResult.setServerResponseMessage("Fatal Error!! #Data persistence");
				return pojoServerResult;
			}
		
			
			// Contatore di scorrimento della prima lista
			int cont = 1;
		
			// Ciclo che crea una lista con la data dell'evento e gli orari associati
			// Esempio. [21/12/2014, 8, 9, 27]
			do{
				// Istanzia una nuova lista
				list.add(new LinkedList<String>());			
			
				String Event_date = resultsetQuery.getString("data_evento");
			
				// Inserisce la data dell'evento nella lista
				list.get(cont).add(Event_date);
			
				
				////////////////////////////////////////////////////////////////
				////////////////////////////////////////////////////////////////
				// Esegue la SELECT [Tabella: Orari]
				
				arraylistQueryRecords.clear();
				arraylistQueryRecords.add(EventID);
				arraylistQueryRecords.add(Event_date);
				
				// ResultSet se � stato trovato il record nella tabella
				// "null" se non esiste il record nella tabella
				ResultSet resultsetHours = Database.select_query(
							"SELECT * FROM Orari WHERE id = ? AND data_evento = ?",
							2,
							arraylistQueryRecords);
				
				// Se il risultato � null allora ad una data non sono associati orari, ed � un errore gravissimo 
				if( resultsetHours == null){
					pojoServerResult.setServerResponse(0);
					pojoServerResult.setServerResponseMessage("Fatal Error!! #Data persistence");
					return pojoServerResult;
				}
				////////////////////////////////////////////////////////////////
				////////////////////////////////////////////////////////////////
			

				// Ciclo che inserisce gli orari associati ad una data[Event_date] nella lista
				do{
					String Event_hour = resultsetHours.getString("ora_evento");
					list.get(cont).add(Event_hour);
				}while(resultsetHours.next() && resultsetQuery.next());
				
				// Contemporaneamente a questa condizione viene spostato avanti il cursore
				cont++;
			
			// Finch� esistono record
			}while(resultsetQuery.next());
				
		} catch (SQLException e) {
			pojoServerResult.setServerResponse(0);
			pojoServerResult.setServerResponseMessage("Fatal Error!! #SQLException");
			return pojoServerResult;
		}
		
		// Arrivando in questo punto del codice, allora l'utente non esiste oppure la password � errata
		pojoServerResult.setServerResponse(1);
		pojoServerResult.setServerResponseMessage("Event finded");
		pojoServerResult.setServerResponseList(list);
		return pojoServerResult;	
	}
}
