package sweng.server;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;

import sweng.client.services.PartecipantDetailService;
import sweng.shared.Database;
import sweng.shared.POJO;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;


/* Classe server-side che ritorna l'username e nome di ogni utente che partecipa ad un determinato evento
 * con data e ora fissati */
@SuppressWarnings("serial")
public class PartecipantDetailServiceImpl extends RemoteServiceServlet implements PartecipantDetailService {

	public POJO partecipant_detailServer(String[] input) throws IllegalArgumentException {
		
		// Oggetto che conterr� i dati di risposta del server
		POJO pojoServerResult = new POJO();
		
		String EventID = input[0];
		String Date = input[1];
		String Hour = input[2];
		
		// ArrayList che conterr� i parametri da inserire nella query
		ArrayList<String> arraylistQueryRecords = new ArrayList<String>();

		LinkedList<LinkedList<String>> list = new LinkedList<LinkedList<String>>();
		
		///////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////
		
		ResultSet stringQuery_result = null;
		
		arraylistQueryRecords.clear();
		arraylistQueryRecords.add(EventID);
		arraylistQueryRecords.add(Date);
		arraylistQueryRecords.add(Hour);
		
		// Esegue la SELECT [Tabella: Partecipazione_Evento]
		// ResultSet se � stato trovato il record nella tabella
		// "null" se non esiste il record nella tabella
		stringQuery_result = Database.select_query(
					"SELECT * FROM Partecipazione_Evento WHERE id = ? AND data_evento = ? AND ora_evento = ?",
					3,
					arraylistQueryRecords);
		

		if( stringQuery_result != null){
			
			try {	
				
				int cont = 0;
				String username = null;
				String nome = null;
				
				do{
					list.add(new LinkedList<String>());
					
					username = stringQuery_result.getString("username");
					nome = stringQuery_result.getString("nome");
					
					list.get(cont).add(nome);
					
					// Se l'utente che partecipa all'evento � registrato aggiungo anche l'username
					if(username != null){
						list.get(cont).add(username);
					}
					
					cont++;
					
				}while(stringQuery_result.next());
				
				pojoServerResult.setServerResponse(1);
				pojoServerResult.setServerResponseMessage("ok");
				pojoServerResult.setServerResponseList(list);
				return pojoServerResult;
			
			} catch (SQLException e) {
				pojoServerResult.setServerResponse(0);
				pojoServerResult.setServerResponseMessage("Fatal error! #SQL Exception");
				return pojoServerResult;
			}
		}
		
		///////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////
		

		pojoServerResult.setServerResponse(0);
		pojoServerResult.setServerResponseMessage("No partecipants yet.");
		return pojoServerResult;
	}
}

