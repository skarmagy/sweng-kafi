package sweng.server;


import java.sql.ResultSet;
import java.util.ArrayList;

import sweng.client.services.CheckIfUserIsPartecipantService;
import sweng.shared.Database;
import sweng.shared.POJO;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;


/* Clase che controlla se l'utente sta gi� partecipando all'evento (con data e ora fissati) */
@SuppressWarnings("serial")
public class CheckIfUserIsPartecipantServiceImpl extends RemoteServiceServlet implements CheckIfUserIsPartecipantService {

	public POJO check_if_user_is_partecipantServer(String[] data) throws IllegalArgumentException {
		
		// Oggetto che conterr� i dati di risposta del server
		POJO pojoServerResult = new POJO();
		
		if( data == null){
			pojoServerResult.setServerResponse(0);
			pojoServerResult.setServerResponseMessage("FATAL ERROR: Data is null");
			return pojoServerResult;
		}
		
		String id = data[0];
		String username = data[1];
		String date = data[2];
		String hour = data[3];
		
		// ArrayList che conterr� i parametri da inserire nella query
		ArrayList<String> arraylistQueryRecords = new ArrayList<String>();

		
		///////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////
		
		ResultSet stringQuery_result = null;
		
		arraylistQueryRecords.clear();
		arraylistQueryRecords.add(id);
		arraylistQueryRecords.add(username);
		arraylistQueryRecords.add(date);
		arraylistQueryRecords.add(hour);
		
		// Esegue la SELECT [Tabella: Partecipazione_Evento]
		// ResultSet se � stato trovato il record nella tabella
		// "null" se non esiste il record nella tabella
		stringQuery_result = Database.select_query(
					"SELECT * FROM Partecipazione_Evento "
					+ "WHERE "
					+ "id = ? AND "
					+ "username = ? AND "
					+ "data_evento = ? AND "
					+ "ora_evento = ?",
					4,
					arraylistQueryRecords);
		
		if( stringQuery_result != null){
				pojoServerResult.setServerResponse(0);
				pojoServerResult.setServerResponseMessage("user has already join this event");
				return pojoServerResult;
		}
		
		///////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////
		

		pojoServerResult.setServerResponse(1);
		pojoServerResult.setServerResponseMessage("ok");
		return pojoServerResult;
	}
}