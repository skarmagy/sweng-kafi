package sweng.server;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import sweng.client.services.PartecipantService;
import sweng.shared.Database;
import sweng.shared.POJO;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;


/* Classe server-side che restituisce il numero di persone che partecipano ad un determinato evento
 * (con data e ora fissati) */

@SuppressWarnings("serial")
public class PartecipantServiceImpl extends RemoteServiceServlet implements PartecipantService {

	public POJO partecipantServer(String[] input) throws IllegalArgumentException {
		
		// Oggetto che conterr� i dati di risposta del server
		POJO pojoServerResult = new POJO();
		
		String id = input[0];
		String date = input[1];
		String hour = input[2];
		
		// ArrayList che conterr� i parametri da inserire nella query
		ArrayList<String> arraylistQueryRecords = new ArrayList<String>();

		
		///////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////
		
		ResultSet stringQuery_result = null;
		
		arraylistQueryRecords.clear();
		arraylistQueryRecords.add(id);
		arraylistQueryRecords.add(date);
		arraylistQueryRecords.add(hour);
		
		// Esegue la SELECT [Tabella: Partecipazione_Evento]
		// ResultSet se � stato trovato il record nella tabella
		// "null" se non esiste il record nella tabella
		stringQuery_result = Database.select_query(
					"SELECT * FROM Partecipazione_Evento WHERE id = ? AND data_evento = ? AND ora_evento = ?",
					3,
					arraylistQueryRecords);
		
		// Se il risultato della query � null significa che l'utente ha inserito le credenziali corrette
		// visto che null indica che nessun record � presente nella tabella con quei dati
		if( stringQuery_result != null){
			try {	
				int cont = 0;
				
				do{
					cont++;
				}while(stringQuery_result.next());
				
				pojoServerResult.setServerResponse(1);
				pojoServerResult.setServerResponseMessage(Integer.toString(cont));
				return pojoServerResult;
			
			} catch (SQLException e) {
				pojoServerResult.setServerResponse(0);
				pojoServerResult.setServerResponseMessage("Fatal error! #SQL Exception");
				return pojoServerResult;
			}
		}
		
		///////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////
		

		pojoServerResult.setServerResponse(1);
		pojoServerResult.setServerResponseMessage("0");
		return pojoServerResult;
	}
}
