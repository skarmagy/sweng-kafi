package sweng.server;

import java.util.ArrayList;

import sweng.client.services.CreateCommentService;
import sweng.shared.Database;
import sweng.shared.POJO;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;


/* Classe server-side che inserisce il commento inserito dall'utente dentro al database */

@SuppressWarnings("serial")
public class CreateCommentServiceImpl extends RemoteServiceServlet implements CreateCommentService {
	
	public POJO create_commentServer(String[] data) throws IllegalArgumentException {
		
		// Oggetto che conterr� i dati di risposta del server
		POJO pojoServerResult = new POJO();
		
		if(data == null){
			pojoServerResult.setServerResponse(0);
			pojoServerResult.setServerResponseMessage("FATAL ERROR: Data is null");
			return pojoServerResult;
		}
		
		String EventID = data[0];
		String Comment = data[1];
		String Username = data[2];
		
		// ArrayList che conterr� i parametri da inserire nella query
		ArrayList<String> arraylistQueryRecords = new ArrayList<String>();
		
		///////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////

		arraylistQueryRecords.clear();
		arraylistQueryRecords.add(EventID);
		arraylistQueryRecords.add(Comment);
		arraylistQueryRecords.add(Username);
		
		// Esegue la INSERT
		// "ok" se la query � andata a buon fine
		// "errore" altrimenti
		String stringQuery_result2 = Database.insert_query(
					"INSERT INTO Commenti(id,commento,username) VALUES(?,?,?)",
					3,
					arraylistQueryRecords);
		
		// Query fallita, ritorna l'eventuale errore
		if( stringQuery_result2 != "ok"){
			pojoServerResult.setServerResponse(0);
			pojoServerResult.setServerResponseMessage(stringQuery_result2);
			return pojoServerResult;
		}
		
		///////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////
		
		pojoServerResult.setServerResponse(1);
		pojoServerResult.setServerResponseMessage("ok");
		return pojoServerResult;
	}
}
