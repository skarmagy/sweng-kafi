package sweng.server;

import sweng.client.services.JoinTheEventService;
import sweng.shared.Database;
import sweng.shared.POJO;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;


/* Classe che inserisce l'utente come partecipante all'evento (con data e ora fissati) */

@SuppressWarnings("serial")
public class JoinTheEventServiceImpl extends RemoteServiceServlet implements JoinTheEventService {

	public POJO join_the_eventServer(LinkedList<LinkedList<String>> List) throws IllegalArgumentException {
		
		// Oggetto che conterr� i dati di risposta del server
		POJO pojoServerResult = new POJO();
		
		if(List == null){
			pojoServerResult.setServerResponse(0);
			pojoServerResult.setServerResponseMessage("FATAL ERROR: LIST IS EMPTY");
			return pojoServerResult;
		}
		
		
		// ArrayList che conterr� i parametri da inserire nella query
		ArrayList<String> arraylistQueryRecords = new ArrayList<String>();
		
		
		// Creo un iteratore alla lista principale che servir� per scorrere gli elementi della lista
		Iterator<LinkedList<String>> MainList_iter = List.listIterator();
		
		// Punto l'iteratore della lista principale alla prima lista
		LinkedList<String> DinamicList = MainList_iter.next();
		
		// Creo un iteratore alla lista figlia puntata dall'iteratore principale
		Iterator<String> SubList_iter = DinamicList.listIterator();
		
		String EventID = SubList_iter.next();
		String Username = SubList_iter.next();
		String Name = SubList_iter.next();
		
		do{

			DinamicList = MainList_iter.next();
			SubList_iter = DinamicList.listIterator();
		
			///////////////////////////////////////////////////////////////////////
			///////////////////////////////////////////////////////////////////////

			arraylistQueryRecords.clear();
			arraylistQueryRecords.add(EventID);
			arraylistQueryRecords.add(Username);
			arraylistQueryRecords.add(Name);
			arraylistQueryRecords.add(SubList_iter.next());
			arraylistQueryRecords.add(SubList_iter.next());
			
			// Esegue la INSERT
			// "ok" se la query � andata a buon fine
			// "errore" altrimenti
			String stringQuery_result2 = Database.insert_query(
						"INSERT INTO Partecipazione_Evento(id,username,nome,data_evento,ora_evento) VALUES(?,?,?,?,?)",
						5,
						arraylistQueryRecords);
			
			// Query fallita, ritorna l'eventuale errore
			if( stringQuery_result2 != "ok"){
				pojoServerResult.setServerResponse(0);
				pojoServerResult.setServerResponseMessage(stringQuery_result2);
				return pojoServerResult;
			}
			
			///////////////////////////////////////////////////////////////////////
			///////////////////////////////////////////////////////////////////////

		}while(MainList_iter.hasNext());
		
		// Query eseguite con successo
		pojoServerResult.setServerResponse(1);
		pojoServerResult.setServerResponseMessage("ok");
		return pojoServerResult;
	}
}
