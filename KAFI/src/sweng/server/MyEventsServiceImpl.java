package sweng.server;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;

import sweng.client.services.MyEventsService;
import sweng.shared.Database;
import sweng.shared.POJO;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;


/* Classe server-side che elabora i dati ricevuti dall'istanza della classe HandlerCreateEvent 
 * Ritorna la lista degli eventi creati dall'utente 
 * */

@SuppressWarnings("serial")
public class MyEventsServiceImpl extends RemoteServiceServlet implements MyEventsService {
	
	public POJO myeventsServer(String username) throws IllegalArgumentException {
		
		// Oggetto che conterr� i dati di risposta del server
		POJO pojoServerResult = new POJO();
		
		// Lista di liste che conterr� tutti gli eventi e i loro parametri associati
		LinkedList<LinkedList<String>> list = new LinkedList<LinkedList<String>>();
		
		// ArrayList che conterr� i parametri da inserire nella query
		ArrayList<String> arraylistQueryRecords = new ArrayList<String>();
		
		ResultSet resultsetQuery_result = null;
		
		arraylistQueryRecords.clear();
		arraylistQueryRecords.add(username);

		// Esegue la SELECT [Tabella: Eventi]
		resultsetQuery_result = Database.select_query(
					"SELECT * FROM Eventi WHERE username = ?",
					1,
					arraylistQueryRecords);
		
		// Se non viene trovato nemmeno un record
		if( resultsetQuery_result == null){
			pojoServerResult.setServerResponse(0);
			pojoServerResult.setServerResponseMessage("You don't have any available event. Try to create a new event.");
			return pojoServerResult;
		}
		
		// Contatore che serve per scorrere la prima lista della lista di liste
		int cont = 0;
		
		try {
			do{
				// Crea una nuova lista
				list.add(new LinkedList<String>());	
				
				// Riempie la lista appena creata
				list.get(cont).add(resultsetQuery_result.getString("id"));
				list.get(cont).add(resultsetQuery_result.getString("nome_evento"));
				list.get(cont).add(resultsetQuery_result.getString("aperto"));
				
				// Se non ci sono altre righe termina la creazione della lista
				if(resultsetQuery_result.isLast()){
					break;
				}
				
				cont++;
				
			}while(resultsetQuery_result.next());
			
		} catch (SQLException e) {
			pojoServerResult.setServerResponse(0);
			pojoServerResult.setServerResponseMessage("Database Fatal error!!");
			return pojoServerResult;
		}

		
		// Query eseguita con successo, viene ritornata la lista di liste con gli eventi creati all'utente
		pojoServerResult.setServerResponse(1);
		pojoServerResult.setServerResponseMessage("Evento/i trovato/i");
		pojoServerResult.setServerResponseList(list);
		return pojoServerResult;
	}
}

