package sweng.server;

import sweng.client.services.LoginService;
import sweng.shared.Database;
import sweng.shared.FieldVerifier;
import sweng.shared.POJO;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;


/* Classe server-side che elabora i dati ricevuti dal popup di Login 
 * Viene verificata la form di login
 * */

@SuppressWarnings("serial")
public class LoginServiceImpl extends RemoteServiceServlet implements LoginService {
	
	public POJO loginServer(String[] input) throws IllegalArgumentException {
		
		// Oggetto che conterr� i dati di risposta del server
		POJO pojoServerResult = new POJO();

		// Eseguo la validazione dei parametri inseriti nella form del popup di Login
		String stringResultOfLoginValidation = FieldVerifier.Login_validate_server_side(input); 
		
		// Se il risultato della validazione non � un "ok" allora restituisco l'errore
		if(stringResultOfLoginValidation != "ok"){
			pojoServerResult.setServerResponse(0);
			pojoServerResult.setServerResponseMessage(stringResultOfLoginValidation);
			return pojoServerResult;
		}
		
		// ArrayList che conterr� i parametri da inserire nella query
		ArrayList<String> arraylistQueryRecords = new ArrayList<String>();
		String username = input[0];
		String password = input[1];
		
		
		///////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////
		
		ResultSet stringQuery_result = null;
		
		arraylistQueryRecords.clear();
		arraylistQueryRecords.add(username);
		arraylistQueryRecords.add(password);
		
		// Esegue la SELECT [Tabella: Login]
		// ResultSet se � stato trovato il record nella tabella
		// "null" se non esiste il record nella tabella
		stringQuery_result = Database.select_query(
					"SELECT * FROM Login WHERE username = ? AND password = ?",
					2,
					arraylistQueryRecords);
		
		// Se il risultato della query � null significa che l'utente ha inserito le credenziali corrette
		// visto che null indica che nessun record � presente nella tabella con quei dati
		if( stringQuery_result != null){
			
			try {
				
				LinkedList<LinkedList<String>> List = new LinkedList<LinkedList<String>>();
				
				String name = stringQuery_result.getString("nome");
				
				List.add(new LinkedList<String>());
				List.get(0).add(username);
				List.get(0).add(name);
				
				pojoServerResult.setServerResponse(1);
				pojoServerResult.setServerResponseMessage("ok");
				pojoServerResult.setServerResponseList(List);
				return pojoServerResult;
			
			} catch (SQLException e) {
				pojoServerResult.setServerResponse(0);
				pojoServerResult.setServerResponseMessage("Fatal error! #SQL");
				return pojoServerResult;
			}
		}
		
		///////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////
		
		
		// Arrivando in questo punto del codice, allora l'utente non esiste oppure la password � errata
		pojoServerResult.setServerResponse(0);
		pojoServerResult.setServerResponseMessage("Wrong Username or Password");
		return pojoServerResult;
	}
}

