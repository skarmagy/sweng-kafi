package sweng.server;

import sweng.client.services.RegisterService;
import sweng.shared.Database;
import sweng.shared.FieldVerifier;
import sweng.shared.POJO;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.LinkedList;


/* Classe server-side che elabora i dati ricevuti dal popup di Registrazione */

@SuppressWarnings("serial")
public class RegisterServiceImpl extends RemoteServiceServlet implements RegisterService {

	public POJO registerServer(String[] input) throws IllegalArgumentException {
		
		// Oggetto che conterr� i dati di risposta del server
		POJO pojoServerResult = new POJO();
		
		// Eseguo la validazione dei parametri inseriti nella form del popup di Registration
		String stringResultOfLoginValidation = FieldVerifier.Register_validate_server_side(input); 
		
		// Se il risultato della validazione non � un "ok" allora restituisco l'errore
		if(stringResultOfLoginValidation != "ok"){
			pojoServerResult.setServerResponse(0);
			pojoServerResult.setServerResponseMessage(stringResultOfLoginValidation);
			return pojoServerResult;
		}
		
		// stringQuery_result conterr� un errore se la query non ha avuto successo
		ResultSet stringQuery_result = null;
		
		// ArrayList che conterr� i parametri da inserire nella query
		ArrayList<String> arraylistQueryRecords = new ArrayList<String>();
		
		String username = input[0];
		String password = input[1];
		String name = input[3];
		String email = input[4];
		
		if(email.length() == 0)
			email = null;
		
		///////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////
		
		// Viene controllato che l'username inserito non sia gi� registrato	

		arraylistQueryRecords.clear();
		arraylistQueryRecords.add(username);
		
		// Esegue la SELECT [Tabella: Login] 
		// ResultSet se � stato trovato il record nella tabella
		// "null" se non esiste il record nella tabella
		stringQuery_result = Database.select_query(
					"SELECT * FROM Login WHERE username = ?",
					1,
					arraylistQueryRecords);
		
		// Se il risultato della query non � null significa che l'utente risulta gi� registrato
		// visto che null indica che nessun record � presente nella tabella con quei dati
		if( stringQuery_result != null){
			pojoServerResult.setServerResponse(0);
			pojoServerResult.setServerResponseMessage("Username already exists");
			return pojoServerResult;
		}
		
		///////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////
		
		// Utente non registrato, registrazione in atto

		arraylistQueryRecords.clear();
		arraylistQueryRecords.add(username);
		arraylistQueryRecords.add(name);
		arraylistQueryRecords.add(password);
		arraylistQueryRecords.add(email);
		
		// Esegue la INSERT
		// "ok" se la query � andata a buon fine
		// "errore" altrimenti
		String stringQuery_result2 = Database.insert_query(
					"INSERT INTO Login(username,nome,password,email) VALUES(?,?,?,?)",
					4,
					arraylistQueryRecords);
		
		// Query fallita, ritorna l'eventuale errore
		if( stringQuery_result2 != "ok"){
			pojoServerResult.setServerResponse(0);
			pojoServerResult.setServerResponseMessage(stringQuery_result2);
			return pojoServerResult;
		}
		
		///////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////
		

		LinkedList<LinkedList<String>> List = new LinkedList<LinkedList<String>>();
		
		List.add(new LinkedList<String>());
		List.get(0).add(username);
		List.get(0).add(name);
		
		pojoServerResult.setServerResponse(1);
		pojoServerResult.setServerResponseMessage("ok");
		pojoServerResult.setServerResponseList(List);
		return pojoServerResult;
	}
}
