package sweng.server;

import java.util.ArrayList;

import sweng.client.services.CloseEventService;
import sweng.shared.Database;
import sweng.shared.POJO;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;


/* Classe server-side che chiude l'evento selezionato e inserisce nel database anche la nota 
 * eventualmente scritta dall'utente */

@SuppressWarnings("serial")
public class CloseEventServiceImpl extends RemoteServiceServlet implements CloseEventService {
	
	public POJO closeeventServer(String[] data) throws IllegalArgumentException {
		
		// Oggetto che conterr� i dati di risposta del server
		POJO pojoServerResult = new POJO();
		
		if(data == null){
			pojoServerResult.setServerResponse(0);
			pojoServerResult.setServerResponseMessage("FATAL ERROR: Data is null");
			return pojoServerResult;
		}
		
		// Lista che contiene i parametri della query
		ArrayList<String> List_db_query = new ArrayList<String>();

		String EventID = data[0];
		String Note = data[1];

		
		List_db_query.clear();
		List_db_query.add(Note);
		List_db_query.add(EventID);

		
		// Esegue la UPDATE [Tabella: Eventi]
		// "ok" se ha avuto successo
		// restituisce l'errore altrimenti
		String stringQuery_result = Database.update_query(
					"UPDATE Eventi "
					+ "SET aperto='0', nota=? WHERE id=?",
					2,
					List_db_query);
		
		// Controllo se la query ha avuto successo, altrimenti restituisce l'errore associato
		if( stringQuery_result != "ok"){
			pojoServerResult.setServerResponse(0);
			pojoServerResult.setServerResponseMessage(stringQuery_result);
			return pojoServerResult;
		}
		
		// Query eseguita con successo
		pojoServerResult.setServerResponse(1);
		pojoServerResult.setServerResponseMessage("ok");
		return pojoServerResult;
	}
}
