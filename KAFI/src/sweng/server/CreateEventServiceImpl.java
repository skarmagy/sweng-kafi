package sweng.server;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

import sweng.client.services.CreateEventService;
import sweng.shared.Database;
import sweng.shared.FieldVerifier;
import sweng.shared.Helper;
import sweng.shared.POJO;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;


/* Classe server-side che elabora i dati ricevuti dall'istanza della classe HandlerCreateEvent
 *  Data una lista di liste il server la elabora verificando se � valida, ed esegue le query sulle tabelle :
 * - Orari: vengono inseriti gli id, orari e date
 * - Eventi: viene inserito l'id, title, place, description, username, ...
 */

@SuppressWarnings("serial")
public class CreateEventServiceImpl extends RemoteServiceServlet implements CreateEventService {
	
	public POJO createeventServer(LinkedList<LinkedList<String>> input) throws IllegalArgumentException {
		
		// Oggetto che conterr� i dati di risposta del server
		POJO pojoServerResult = new POJO();
		
		// Data una [lista di liste] si verifica l'integrit� dei dati passati dalla form CreateEvent
		// Se il risultato della chiamata � un "ok" bene, altrimenti viene restituito l'errore
		String stringValidation_result = FieldVerifier.CreateEvent_validate_server_side(input);
		
		if( stringValidation_result != "ok"){
			pojoServerResult.setServerResponse(0);
			pojoServerResult.setServerResponseMessage(stringValidation_result);
			return pojoServerResult;
		}
		
		
		// Creo un iteratore alla lista principale che servir� per scorrere gli elementi della lista
		Iterator<LinkedList<String>> MainList_iter = input.listIterator();
		
		// Punto l'iteratore della lista principale alla prima lista
		LinkedList<String> DinamicList = MainList_iter.next();
		
		// Creo un iteratore alla lista figlia puntata dall'iteratore principale
		Iterator<String> SubList_iter = DinamicList.listIterator();
				
		String EventUsername = SubList_iter.next();
		String EventName = SubList_iter.next();
		String EventPlace = SubList_iter.next();
		String EventDescription = SubList_iter.next();
		String EventID = Helper.generate_random_string(); // Genero un identificativo univoco
		String EventDate;
		String EventHours;

		ArrayList<String> List_db_query = new ArrayList<String>();
		
		// Ciclo do-while che inserisce le date e gli orari della form "CreateEvent" nella tabella Orari
		do{
			// Posizioniamo l'iteratore alla lista successiva, quella che contiene la
			// la data dell'evento e i suoi orari
			DinamicList = MainList_iter.next();
			SubList_iter = DinamicList.listIterator();
			
			// Stringa che contiene la data dell'evento
			EventDate = SubList_iter.next();

			do{
				EventHours = SubList_iter.next();		
				
				List_db_query.clear();
				List_db_query.add(EventID);
				List_db_query.add(EventDate);
				List_db_query.add(EventHours);
				
				// Esegue la INSERT [Tabella: Orari]
				// "ok" se ha avuto successo
				// restituisce l'errore altrimenti
				String stringQuery_result = Database.insert_query(
							"INSERT INTO Orari(id,data_evento,ora_evento) VALUES(?,?,?)",
							3,
							List_db_query);
				
				// Controllo se la query ha avuto successo, altrimenti restituisce l'errore associato
				if( stringQuery_result != "ok"){
					pojoServerResult.setServerResponse(0);
					pojoServerResult.setServerResponseMessage(stringQuery_result);
					return pojoServerResult;
				}
				
			}while(SubList_iter.hasNext());	
			
		}while(MainList_iter.hasNext());
		
		
		List_db_query.clear();
		List_db_query.add(EventID);
		List_db_query.add(EventName);
		List_db_query.add(EventPlace);
		List_db_query.add(EventDescription);
		List_db_query.add(EventUsername);
		List_db_query.add("1");
		
		// Esegue la INSERT [Tabella: Eventi] 
		// "ok" se ha avuto successo
		// restituisce l'errore altrimenti
		String stringQuery_result = Database.insert_query(
					"INSERT INTO Eventi(id,nome_evento,luogo_evento,descrizione_evento,username,aperto) VALUES(?,?,?,?,?,?)",
					6,
					List_db_query);
		
		// Controllo se la query ha avuto successo, altrimenti restituisce l'errore associato
		if( stringQuery_result != "ok"){
			pojoServerResult.setServerResponse(0);
			pojoServerResult.setServerResponseMessage(stringQuery_result);
			return pojoServerResult;
		}
		
		// La query ha avuto successo
		pojoServerResult.setServerResponse(1);
		pojoServerResult.setServerResponseMessage(EventID);
		return pojoServerResult;
	}
}
