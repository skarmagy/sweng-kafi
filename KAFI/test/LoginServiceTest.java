import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import sweng.server.LoginServiceImpl;
import sweng.shared.POJO;


public class LoginServiceTest {

	private LoginServiceImpl test = new LoginServiceImpl();
	private String[] arrayRegister_values = new String[5];
	
	@Test
	public void testing() {
		
		arrayRegister_values[0] = "username";
		arrayRegister_values[1] = "password";
		
		POJO pojoServerResult = test.loginServer(arrayRegister_values);
		
		int pojoServerResult_response = pojoServerResult.getServerResponse();
		String pojoServerResult_message = pojoServerResult.getServerResponseMessage();

		assertNotNull(pojoServerResult);
		assertEquals(0, pojoServerResult_response);
		assertEquals("Wrong Username or Password", pojoServerResult_message);
	}
}
