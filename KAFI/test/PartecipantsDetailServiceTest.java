import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import sweng.server.PartecipantDetailServiceImpl;
import sweng.shared.POJO;


public class PartecipantsDetailServiceTest {
	
	private PartecipantDetailServiceImpl test = new PartecipantDetailServiceImpl();
	private String[] array_values = new String[3];
	
	@Test
	public void testing() {
		
		array_values[0] = "EVENT ID";
		array_values[1] = "20/12/2015";
		array_values[2] = "22.30";
		
		POJO pojoServerResult = test.partecipant_detailServer(array_values);
		
		int pojoServerResult_response = pojoServerResult.getServerResponse();
		String pojoServerResult_message = pojoServerResult.getServerResponseMessage();

		assertNotNull(pojoServerResult);
		assertEquals(0, pojoServerResult_response);
		assertEquals("No partecipants yet.", pojoServerResult_message);
	}
}
