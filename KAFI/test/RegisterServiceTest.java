import static org.junit.Assert.*;

import org.junit.Test;

import sweng.server.RegisterServiceImpl;
import sweng.shared.POJO;


public class RegisterServiceTest {

	private RegisterServiceImpl test = new RegisterServiceImpl();
	private String[] arrayRegister_values = new String[5];
	
	@Test
	public void test_username_too_short() {
		
		String username = "";
		String password = "password";
		String confirmPassword = "password";
		String name = "name";
		String email = "email";
		
		arrayRegister_values[0] = username;
		arrayRegister_values[1] = password;
		arrayRegister_values[2] = confirmPassword;
		arrayRegister_values[3] = name;
		arrayRegister_values[4] = email;
		
		POJO pojoServerResult = test.registerServer(arrayRegister_values);
		
		int pojoServerResult_response = pojoServerResult.getServerResponse();
		String pojoServerResult_message = pojoServerResult.getServerResponseMessage();

		assertNotNull(pojoServerResult);
		assertEquals(0, pojoServerResult_response);
		assertEquals("Username too short (min 7)", pojoServerResult_message);
	}
	
	@Test
	public void test_username_too_long() {
		
		String username = "123456789123456789123456789";
		String password = "password";
		String confirmPassword = "password";
		String name = "name";
		String email = "email";
		
		arrayRegister_values[0] = username;
		arrayRegister_values[1] = password;
		arrayRegister_values[2] = confirmPassword;
		arrayRegister_values[3] = name;
		arrayRegister_values[4] = email;
		
		POJO pojoServerResult = test.registerServer(arrayRegister_values);
		
		int pojoServerResult_response = pojoServerResult.getServerResponse();
		String pojoServerResult_message = pojoServerResult.getServerResponseMessage();

		assertNotNull(pojoServerResult);
		assertEquals(0, pojoServerResult_response);
		assertEquals("Username too long (max 15)", pojoServerResult_message);
	}
	
	@Test
	public void test_password_do_not_match() {
		
		String username = "username";
		String password = "";
		String confirmPassword = "mypassword";
		String name = "name";
		String email = "email";
		
		arrayRegister_values[0] = username;
		arrayRegister_values[1] = password;
		arrayRegister_values[2] = confirmPassword;
		arrayRegister_values[3] = name;
		arrayRegister_values[4] = email;
		
		POJO pojoServerResult = test.registerServer(arrayRegister_values);
		
		int pojoServerResult_response = pojoServerResult.getServerResponse();
		String pojoServerResult_message = pojoServerResult.getServerResponseMessage();

		assertNotNull(pojoServerResult);
		assertEquals(0, pojoServerResult_response);
		assertEquals("Passwords do not match", pojoServerResult_message);
	}
	
	@Test
	public void test_password_too_short() {
		
		String username = "username";
		String password = "1234";
		String confirmPassword = "1234";
		String name = "name";
		String email = "email";
		
		arrayRegister_values[0] = username;
		arrayRegister_values[1] = password;
		arrayRegister_values[2] = confirmPassword;
		arrayRegister_values[3] = name;
		arrayRegister_values[4] = email;
		
		POJO pojoServerResult = test.registerServer(arrayRegister_values);
		
		int pojoServerResult_response = pojoServerResult.getServerResponse();
		String pojoServerResult_message = pojoServerResult.getServerResponseMessage();

		assertNotNull(pojoServerResult);
		assertEquals(0, pojoServerResult_response);
		assertEquals("Password too short (min 7)", pojoServerResult_message);
	}
	
	@Test
	public void test_password_too_long() {
		
		String username = "username";
		String password = "123456789123456789";
		String confirmPassword = "123456789123456789";
		String name = "name";
		String email = "email";
		
		arrayRegister_values[0] = username;
		arrayRegister_values[1] = password;
		arrayRegister_values[2] = confirmPassword;
		arrayRegister_values[3] = name;
		arrayRegister_values[4] = email;
		
		POJO pojoServerResult = test.registerServer(arrayRegister_values);
		
		int pojoServerResult_response = pojoServerResult.getServerResponse();
		String pojoServerResult_message = pojoServerResult.getServerResponseMessage();

		assertNotNull(pojoServerResult);
		assertEquals(0, pojoServerResult_response);
		assertEquals("Password too long (max 15)", pojoServerResult_message);
	}
	
	@Test
	public void test_name_too_short() {
		
		String username = "username";
		String password = "password";
		String confirmPassword = "password";
		String name = "";
		String email = "email";
		
		arrayRegister_values[0] = username;
		arrayRegister_values[1] = password;
		arrayRegister_values[2] = confirmPassword;
		arrayRegister_values[3] = name;
		arrayRegister_values[4] = email;
		
		POJO pojoServerResult = test.registerServer(arrayRegister_values);
		
		int pojoServerResult_response = pojoServerResult.getServerResponse();
		String pojoServerResult_message = pojoServerResult.getServerResponseMessage();

		assertNotNull(pojoServerResult);
		assertEquals(0, pojoServerResult_response);
		assertEquals("Name too short (min 3)", pojoServerResult_message);
	}
	
	@Test
	public void test_name_too_long() {
		
		String username = "username";
		String password = "password";
		String confirmPassword = "password";
		String name = "123456789123456789";
		String email = "email";
		
		arrayRegister_values[0] = username;
		arrayRegister_values[1] = password;
		arrayRegister_values[2] = confirmPassword;
		arrayRegister_values[3] = name;
		arrayRegister_values[4] = email;
		
		POJO pojoServerResult = test.registerServer(arrayRegister_values);
		
		int pojoServerResult_response = pojoServerResult.getServerResponse();
		String pojoServerResult_message = pojoServerResult.getServerResponseMessage();

		assertNotNull(pojoServerResult);
		assertEquals(0, pojoServerResult_response);
		assertEquals("Name too long (max 15)", pojoServerResult_message);
	}
	
	@Test
	public void test_email_too_long() {
		
		String username = "username";
		String password = "password";
		String confirmPassword = "password";
		String name = "name";
		String email = "123456789123456789123456789123456789@1234567891234567891234567891234567.89";
		
		arrayRegister_values[0] = username;
		arrayRegister_values[1] = password;
		arrayRegister_values[2] = confirmPassword;
		arrayRegister_values[3] = name;
		arrayRegister_values[4] = email;
		
		POJO pojoServerResult = test.registerServer(arrayRegister_values);
		
		int pojoServerResult_response = pojoServerResult.getServerResponse();
		String pojoServerResult_message = pojoServerResult.getServerResponseMessage();

		assertNotNull(pojoServerResult);
		assertEquals(0, pojoServerResult_response);
		assertEquals("Email too long (max 40)", pojoServerResult_message);
	}
	
	@Test
	public void test_email_wrong_format() {
		
		String username = "username";
		String password = "password";
		String confirmPassword = "password";
		String name = "name";
		String email = "email";
		
		arrayRegister_values[0] = username;
		arrayRegister_values[1] = password;
		arrayRegister_values[2] = confirmPassword;
		arrayRegister_values[3] = name;
		arrayRegister_values[4] = email;
		
		POJO pojoServerResult = test.registerServer(arrayRegister_values);
		
		int pojoServerResult_response = pojoServerResult.getServerResponse();
		String pojoServerResult_message = pojoServerResult.getServerResponseMessage();

		assertNotNull(pojoServerResult);
		assertEquals(0, pojoServerResult_response);
		assertEquals("Wrong email format", pojoServerResult_message);
	}
}
