import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import sweng.server.MyEventsServiceImpl;
import sweng.shared.POJO;


public class MyEventsServiceTest {
	
	private MyEventsServiceImpl test = new MyEventsServiceImpl();
	private String username = "username";
	
	@Test
	public void testing() {
		POJO pojoServerResult = test.myeventsServer(username);
		
		int pojoServerResult_response = pojoServerResult.getServerResponse();
		String pojoServerResult_message = pojoServerResult.getServerResponseMessage();

		assertNotNull(pojoServerResult);
		assertEquals(0, pojoServerResult_response);
		assertEquals("You don't have any available event. Try to create a new event.", pojoServerResult_message);
	}
}
