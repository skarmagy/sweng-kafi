import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import sweng.server.DeleteEventServiceImpl;
import sweng.shared.POJO;


public class DeleteEventServiceTest {

	private DeleteEventServiceImpl test = new DeleteEventServiceImpl();
	private String EventID = "EVENT";
	
	@Test
	public void testing() {
		POJO pojoServerResult = test.deleteeventServer(EventID);
		
		int pojoServerResult_response = pojoServerResult.getServerResponse();

		assertNotNull(pojoServerResult);
		assertEquals(0, pojoServerResult_response);
	}
}
