import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import sweng.server.PartecipantServiceImpl;
import sweng.shared.POJO;


public class PartecipantsServiceTest {
	
	private PartecipantServiceImpl test = new PartecipantServiceImpl();
	private String[] array_values = new String[3];
	
	@Test
	public void testing() {
		
		array_values[0] = "EVENT ID";
		array_values[1] = "20/12/2015";
		array_values[2] = "22.30";
		
		POJO pojoServerResult = test.partecipantServer(array_values);
		
		int pojoServerResult_response = pojoServerResult.getServerResponse();
		String pojoServerResult_message = pojoServerResult.getServerResponseMessage();

		assertNotNull(pojoServerResult);
		assertEquals(1, pojoServerResult_response);
		assertEquals("0", pojoServerResult_message);
	}
}
