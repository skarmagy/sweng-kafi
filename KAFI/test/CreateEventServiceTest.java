import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.LinkedList;

import org.junit.Test;

import sweng.server.CreateEventServiceImpl;
import sweng.shared.POJO;


public class CreateEventServiceTest {

	private CreateEventServiceImpl test = new CreateEventServiceImpl();
	private LinkedList<LinkedList<String>> List = null;
	
	@Test
	public void testing() {
		POJO pojoServerResult = test.createeventServer(List);
		
		int pojoServerResult_response = pojoServerResult.getServerResponse();
		String pojoServerResult_message = pojoServerResult.getServerResponseMessage();

		assertNotNull(pojoServerResult);
		assertEquals(0, pojoServerResult_response);
		assertEquals("FATAL ERROR: LIST IS EMPTY", pojoServerResult_message);
	}
}
