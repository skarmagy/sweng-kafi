import static org.junit.Assert.*;

import org.junit.Test;

import sweng.server.SearchEventServiceImpl;
import sweng.shared.POJO;


public class SearchEventTest {

	private SearchEventServiceImpl test = new SearchEventServiceImpl();
	private String EventID = "EVENT";
	
	@Test
	public void testing() {
		POJO pojoServerResult = test.searcheventServer(EventID);
		
		int pojoServerResult_response = pojoServerResult.getServerResponse();
		String pojoServerResult_message = pojoServerResult.getServerResponseMessage();

		assertNotNull(pojoServerResult);
		assertEquals(0, pojoServerResult_response);
		assertEquals("Wrong ID format", pojoServerResult_message);
	}

}
