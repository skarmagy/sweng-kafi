import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import sweng.server.CheckIfUserIsPartecipantServiceImpl;
import sweng.shared.POJO;


public class CheckIfUserIsPartecipantTest {

	private CheckIfUserIsPartecipantServiceImpl test = new CheckIfUserIsPartecipantServiceImpl();
	private String[] array_values = null;
	
	@Test
	public void testing() {
		
		POJO pojoServerResult = test.check_if_user_is_partecipantServer(array_values);
		
		int pojoServerResult_response = pojoServerResult.getServerResponse();
		String pojoServerResult_message = pojoServerResult.getServerResponseMessage();

		assertNotNull(pojoServerResult);
		assertEquals(0, pojoServerResult_response);
		assertEquals("FATAL ERROR: Data is null", pojoServerResult_message);
	}
}
