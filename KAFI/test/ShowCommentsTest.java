import static org.junit.Assert.*;

import org.junit.Test;

import sweng.server.ShowCommentsServiceImpl;
import sweng.shared.POJO;


public class ShowCommentsTest {

	private ShowCommentsServiceImpl test = new ShowCommentsServiceImpl();
	private String EventID = "EVENT";
	
	@Test
	public void testing() {
		POJO pojoServerResult = test.show_commentsServer(EventID);
		
		int pojoServerResult_response = pojoServerResult.getServerResponse();
		String pojoServerResult_message = pojoServerResult.getServerResponseMessage();

		assertNotNull(pojoServerResult);
		assertEquals(0, pojoServerResult_response);
		assertEquals("No comments posted yet.", pojoServerResult_message);
	}

}
